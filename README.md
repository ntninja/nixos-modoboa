# Modoboa NixOS Integration

*User-friendly NixOS mail system setup!*

A NixOS configuration module and package overlay for [Modoboa](https://modoboa.org/)

In order to be useful, this configuration reimplements large parts of the
[modoboa-installer](https://github.com/modoboa/modoboa-installer) for
Debian/Ubuntu. For a more seamless experience it also adds several automations
(`overlay/python-pkgs/modoboa/instance/extras/`) that should probably be
upstreamed to benefit everyone rather than being maintained here…

Differences to the Modoboa configuration from the manual/installer:

 * Caddy is used as web server (patches for nginx/Apache welcome!)
 * Mail submission is handled by Dovecot rather than Postfix (if you use the example configuration below)
 * Amavis is not included (however received messages are scanned by rspamd)
 * DKIM sigining uses rspamd instead of OpenDKIM (unofficially supported by Modoboa upstream)

## Usage

(There is still information on adding all the many different DNS entries
missing here.)

1. Add the modoboa-nixos channel to your system:

   ```shell
   $ sudo nix-channel --add https://gitlab.com/ntninja/modoboa-nixos/-/archive/main/modoboa-nixos-main.tar.bz2 modoboa
   ```

   This adds the contents of this repository as a Nix channel to your system
   and makes sure it is automatically updated when later running
   `sudo nix-channel --update`.

2. Import the modoboa-nixos top-level module in `/etc/nixos/configuration.nix`:

    ```nix
    # …
    
    {
    	# …

    	imports = [
			# …
			<modoboa>  # Yes, it’s really this line
			# …
    	];

    	# …
    }
    ```

3. Generate the Postfix S2S server certificates and print the corresponding TLSA record:

   1. Launch nix shell with required CLI utils:

      ```
      $ nix-shell -p gnutls -p ldns.examples
      ```

   2. Generate a TLS private key:

      ```
      $ sudo certtool --generate-privkey --outfile /etc/modoboa/postfix.key
      ```

   3. Generate a self-signed TLS public key using the private key from the
      previous step:

      ```
      $ sudo certtool --generate-self-signed --load-privkey /etc/modoboa/postfix.key --outfile /etc/modoboa/postfix.crt
      ```

      When prompted for the “common name” and “subject alernative name” of the
      certificate enter the MX host name of the server (what you write into the
      MX DNS record)! Also make sure CA usage is _disabled_ and TLS WWW Server
      usage is _enabled_.

   4. Print the TLSA record to publish (replacing `${MX_HOST_NAME}` with the
      respective host name again):

      ```
      $ ldns-dane create -c /etc/modoboa/postfix.crt "${MX_HOST_NAME}" 25
      ```

      Publish this value as TLSA record on the MX hostname of the mail server.
      Unless you do this messages from other mail servers may be intercepted
      as other mail servers won’t known which TLS certificate to expect!
      (Note that many servers don’t support certificate verification at all, so
      will still allow their messages to be intercepted even if you do this…)

4. Configure Modoboa and associated services:

   This following example uses PostgreSQL as a database (see the comment there
   for other options) and expects the database and database user to already
   exist. Commented-out options should be considered recommendations, you might
   want to look at each of them and decide which ones you’d like to enable –
   make sure you enable one of the Dovecot TLS certificate configurations
   however to make sure your mail setup isn’t completely insecure!

   Some additional advanced options (custom packages, etc) are not mentioned
   below, see `modules/modoboa.nix` for the entire option set.
 
    ```nix
    { config, pkgs, lib, ... }:

    let
    	# List of host names to serve Modoboa (and also Dovecot/Postfix) at
    	#
    	# Make sure that all host names listed here actually point at your
    	# server before continuing!
    	#
    	# Also note that this is not the place to enter the list of e-mail
    	# domains (the part in the e-mail address after the “@”)! This is
    	# _just_ the list of host names that the Modoboa web interface should
    	# accessible at. Managing e-mail domains is done post-installation in
    	# the Modoboa web interface instead.
    	#
    	# Were it isn’t possible to use more than one hostname, only the first
    	# entry in this list will be used. This shouldn’t be noticable to
    	# end-users but may cause unexpected issues if that host name fails to
    	# resolve while other host names are still available.
    	hostNames = [
	    	"mail.example.org"
    	];
    in {
    	services.modoboa = {
    		# The master switch
    		enable = true;
    		
    		settings = {
    			# List of domains allowed access
    			ALLOWED_HOSTS = hostNames;
    			
    			# Database access information
    			#
    			# `ENGINE` may be one of "django.db.backends.postgresql",
    			# "django.db.backends.mysql" or "django.db.backends.sqlite3".
    			#
    			# In case of PostgreSQL or MySQL you must specify at least
    			# the database `NAME` and `USER` name. If the database is not
    			# running on localhost use `HOST` and `PORT` to specify its
    			# location. Use `PASSWORD_FILE` to specify the path to a file,
    			# readable by the “modoboa” user, containing the database
    			# password.
    			#
    			# In case of SQLite, `NAME` may be used to specify the location
    			# of the SQLite database file, defaulting to
    			# "/var/lib/modoboa/database.db".
    			DATABASES.default = {
    				ENGINE = "django.db.backends.postgresql";
    				HOST = "localhost";
    				NAME = "modoboa";
    				USER = "modoboa";
    				PASSWORD_FILE = /etc/modoboa/database.passwd;
    			};

    			# Enable locale-aware number formatting
    			#USE_THOUSAND_SEPARATOR = true;
    		};

    		# Address special-cased by Postfix to handle DMARC reports
    		#
    		# Must be added as `rua=` DMARC value on all domains that should be
    		# included in Modoboa’s DMARC report feature. Setting up this value
    		# correctly makes the DMARC statistics feature of Modoboa useful.
    		#dmarcAddress = "dmarc@example.org";

    		# Enable Caddy web server integration
    		#
    		# Alternatively you can also set up your own web server integration
    		# for the Modoboa web frontend.
    		#caddy.enable = true;

    		# Enable Radicale and rspamd config integration
    		#radicale.enable = true;
    		#rspamd.enable = true;  # Default

    		# Enable additional Modoboa extensions
    		#extensions = [
    		#	"modoboa-contacts"
    		#	"modoboa-postfix-autoreply"
    		#	"modoboa-sievefilters"
    		#	"modoboa-webmail"
    		#];
    	};

    	services.dovecot2 = {
    		# Enable POP3 protocol support in Dovecot
    		#
    		# Depending on your use-case IMAP + Submission might suffice!
    		#enablePop3 = true; 

    		globalSettings = {
    			# Enable Dovecot mail submission proxy
    			#
    			# Dovecot submission proxy transparently replaces Postfix for
    			# clients submitting/sending messages adding additional
    			# features on top of those provided by Postfix.
    			protocols = [ "submission" ];
    			submission_relay_host = "localhost";  # The locally running Postfix
    			submission_relay_trusted = true;

    			# Enable support for the much more efficient IMAP NOTIFY new
    			# message notification mechanism in clients that support it
    			mailbox_list_index = true;

    			# Set path to mail attribute storage file used by some IMAP
    			# extensions
    			mail_attribute_dict = "file:%h/dovecot-attributes";

    			# Enable usage of the `imap-hibernate` process to save resources
    			# for clients blocked in IMAP IDLE
    			imap_hibernate_timeout = "5s";

    			# Increase maximum number of per-IP user connections to allow
    			# clients without IMAP NOTIFY support to idle
    			#mail_max_userip_connections = 100;

    			# Enable support for IMAP URLAUTH extension
    			#
    			# This allows supporting clients to avoid uploading messages
    			# twice when sending by storing them in the IMAP Sent folder
    			# first then referencing the uploaded message using SMTP BURL.
    			#
    			# Note that the Dovecot documentation warns that URLAUTH may
    			# be broken in some ways, however no indication exists in what
    			# ways it is broken in neither the mailing list nor the source
    			# code while apparently many known issues have been fixed in
    			# the past.
    			#imap_urlauth_host = lib.lists.head hostNames;
			
    			# Recipient delimiter separating the username and address
    			# extension in email addresses (has to match Postfix)
    			#
    			# This allows using email addresses of the form
    			# `name+something@example.org` without creating separate
    			# aliases for each subaddress in Modoboa.
    			#recipient_delimiter = "+";
    		};

    		# Allow `imap-hibernate` process to access the `imap` process’ socket
    		services.imap.unixListeners.imap-master = {
    			user = config.services.dovecot2.user; # = "$default_internal_user";
    		};

    		# Add listener for SMTP submission with immediate TLS
    		services.submission-login.inetListeners.submissions = {
    			port = 465;
    			tls = true;
    		};

    		# Limit support for protocols using deprecated STARTTLS-based
    		# encryption to localhost
    		#
    		# Use of STARTTLS-based encryption has been deprecated by RFC 8314
    		# in favour using immediate TLS for protocols that allow both
    		# variants (POP3, IMAP, Submission/SMTP). Note that _correctly
    		# implemented_ STARTTLS configurations are _not insecure_ and
    		# setups that wish to serve clients with legacy configuration
    		# should not disable it without asking users to update their
    		# configuration first, however new setups should consider disabling
    		# it to significantly reduce the attack-surface for plaintext-
    		# downgrade attacks in clients.
    		#services.imap-login.inetListeners.imap = {
    		#	port = 143;
    		#	address = ["127.0.0.1" "::1"];
    		#};
    		#services.pop3-login.inetListeners.pop3 = {
    		#	port = 0;  # Disables protocol listener
    		#};
    		#services.submission-login.inetListeners.submission = {
    		#	port = 587;
    		#	address = ["127.0.0.1" "::1"];
    		#};

			# Dovecot TLS configuration reusing Certbot certificate
			#
			# Note that this expects the first host name to be the “main”
			# host name tracked by Certbot. For Caddy set next item.
    		#sslServerCert = "/etc/letsencrypt/live/${lib.lists.head hostNames}/fullchain.pem";
    		#sslServerKey = "/etc/letsencrypt/live/${lib.lists.head hostNames}/privkey.pem";
    	} // (
    	# Dovecot TLS configuration reusing Caddy certificates
    	#
    	# Note that reusing its certificates like this is probably not
    	# well-supported by Caddy and that some (particularily mobile) still
    	# fail to send the hostname when establishing a TLS connection (SNI),
    	# so will always receive the default certificate. For Certbot see
    	# previous item.
    	#let
    	#	# Caddy TLS certificate directory
    	#	certDir = "/var/lib/caddy/.local/share/caddy/certificates/acme-v02.api.letsencrypt.org-directory";
		#
    	#	defaultHostName = lib.lists.head hostNames;
    	#in {
    	#	# Default TLS certificates served if no SNI is sent by client
    	#	sslServerCert = "${certDir}/${defaultHostName}/${defaultHostName}.crt";
    	#	sslServerKey = "${certDir}/${defaultHostName}/${defaultHostName}.key";
    	#
    	#	# SNI-dependant TLS certificates
    	#	extraConfig = lib.mkAfter (lib.strings.concatLines (lib.lists.map (hostName:
    	#		''
    	#			local_name ${hostName} {
    	#				ssl_cert = <${certDir}/${hostName}/${hostName}.crt
    	#				ssl_key = <${certDir}/${hostName}/${hostName}.key
    	#			}
    	#		''
    	#	) hostNames));
    	#});

    	services.postfix = {
    		# Recipient delimiter separating the username and address extension
    		# in email addresses (has to match Dovecot, for info see there)
    		#recipientDelimiter = "-";

    		config = {
    			# Establish trusted TLS for remote systems supporting DANE,
    			# otherwise use a best-effort (“may”) TLS connection
    			smtp_dns_support_level = "dnssec";
    			smtp_tls_security_level = "dane";
    			smtp_tls_session_cache_database = "btree:$data_directory/smtp_scache";

    			# Trust Dovecot submission proxy when it sends remote client info
    			smtpd_authorized_xclient_hosts = "$mynetworks";
    			
    			# Set fully-qualified domain name
    			#
    			# Not required if your server hostname is already
    			# fully-qualified, otherwise _you have to set this up correctly_
    			# to get any messages delivered correctly on the public
    			# Internet!
    			#myhostname = "vmanager9500.premium-vserver.net";

    			# Prefer delivering messages using IPv4
    			#
    			# Set this if the fully-qualified domain name of your server
    			# does not include the IPv6 address. (Again, your messages
    			# will be rejected if you mess this up!)
    			#smtp_address_preference = "ipv4";

    			# Raise maximum message size (bytes)
    			#
    			# The second value is only relevant for local users, but needs
    			# to be at least as large as the first.
    			#message_size_limit = "100000000";
    			#mailbox_size_limit = "100000000";

    			# Notify admin address on important failures
    			#notify_classes = ["bounce" "data" "delay" "resource" "software"];

    			# Disable support for some legacy Unix new mail notification
    			# tool that isn’t in NixOS
    			#biff = false;

    			# Extra Modoboa TLS settings
    			smtpd_tls_session_cache_database = "btree:$data_directory/smtpd_tls_session_cache";
    			smtpd_tls_received_header = true;
    		};

    		# TLS configuration (self-signed certificate referenced using DANE)
    		#
    		# This is only used for server-to-server message reception, email
    		# clients exclusively talk to Dovecot (POP, IMAP, Submission)
    		# instead.
    		#
    		# Certificate was generated by running the following:
    		#
    		#    certtool --generate-privkey --outfile /etc/modoboa/postfix.key
    		#    certtool --generate-self-signed --load-privkey /etc/modoboa/postfix.key --outfile /etc/modoboa/postfix.crt
    		#
    		# Make sure to specify the _MX host name_ as common name and
    		# subject alternative name and mark it as a non-CA TLS WWW Server
    		# certificate.
    		#
    		# Details on the TLS certificate to use DANE + E-Mail:
    		# https://github.com/internetstandards/toolbox-wiki/blob/d8aded7416f77f8bf6091d5ec249ccc2c285ef0a/DANE-for-SMTP-how-to.md#tips-tricks-and-notices-for-implementation
    		#
    		# The DNS record to export for the generated TLS certificate can be exported
    		# by running:
    		#
    		#    ldns-dane create -c /etc/modoboa/postfix.crt "${MX_HOST_NAME}" 25
    		sslCert = "/etc/modoboa/postfix.crt";
    		sslKey = "/etc/modoboa/postfix.key";
    	};
    }
    ```

5. Run `sudo nixos-rebuild switch`

## Upgrades

Upgrades should be handled automatically by the packaging available here.
If not, there’s a bug! 

## Missing Features

 * Add support for automatically provisioning IMAPSync
 * Add packaging for missing plugins:
    * modoboa-amavis

If you feel like adding these, please open an MR – thanks!

## Getting Help

Feel free to open an issue, but please note that I, @ntninja, may not respond
quickly. Preferably, please open a MR – even if you know it has issues – with
some suggested change instead! (I’d much rather work with you on a solution
that figure out your issues for you… It means someone else can fix it up
later rather than starting from scratch if I don’t have the time/energy
for it.)

Note that feature requests will be closed after being added to the list above.

Note that I won’t try to integrate this into NixOS itself, as I don’t feel
comfortable with taking on the necessary maintainer role. The packaging
should be in a good shape however, so if you’d like to see this added please
feel free!