{ config, pkgs, lib, utils, ... }:

{
	# Add package overlay
	nixpkgs.overlays = [
		(import ./overlay)
	];

	# Include system configuration modules
	imports = [
		./modules/modoboa.nix
		./modules/dovecot.nix  # Patched Dovecot module with extra options
	];
}