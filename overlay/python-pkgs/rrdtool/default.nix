{ lib
, buildPythonPackage
, fetchFromGitHub
, pythonOlder
, python
, rrdtool
, unittestCheckHook
}:

buildPythonPackage rec {
	pname = "rrdtool";
	version = "0.1.16";
	disable = pythonOlder "3.4";

	src = fetchFromGitHub {
		owner = "commx";
		repo = "python-rrdtool";
		rev = "v${version}";
		hash = "sha256-xBMyY2/lY16H7D0JX5BhgHV5afDKKDObPJnynZ4iZdI=";
	};

	nativeBuildInputs = [
	];

	propagatedBuildInputs = [
		rrdtool
	];

	nativeCheckInput = [
		unittestCheckHook
	];

	pythonImportsCheck = [ "rrdtool" ];

	meta = with lib; {
		homepage = "https://github.com/commx/python-rrdtool";
		description = "Python bindings for RRDtool";
		license = licenses.lgpl21Plus;
		maintainers = with maintainers; [ ];
	};
}
