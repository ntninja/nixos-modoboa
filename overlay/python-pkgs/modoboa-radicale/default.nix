{ lib
, buildPythonPackage
, fetchPypi
, python
, pythonOlder
# Pre-dependencies
, pip
, setuptools
, setuptools-scm
# Dependencies
, caldav
, django-filter
, django-webpack-loader
, drf-nested-routers
, modoboa
# Test dependencies
, factory-boy
, packaging
, testfixtures
}:

buildPythonPackage rec {
	pname = "modoboa-radicale";
	version = "1.7.2";
	disabled = pythonOlder "3.8";

	# Note: Source contains pre-built frontend created using WebPack
	src = fetchPypi {
		inherit pname version;
		hash = "sha256-Z6Ft9cbsSCjo7OWgL0DLpJ3OvX+EYIrq3wTQChBPxA4=";
	};
	
	patches = [
		./01-rights-file-allow-web-access.patch
	];

	nativeBuildInputs = [
		pip  # Used in `setup.py` but not declared
		setuptools
		setuptools-scm
	];

	propagatedBuildInputs = [
		caldav
		django-filter
		django-webpack-loader
		drf-nested-routers
		modoboa
	];

	preBuild = ''
		# Release has exact version requirements that do not reflect what is
		# actually needed
		#
		# In addition, for all packages listed in requirements.txt that are
		# not present on the system, the setup.py will attempt to auto-install
		# them from PyPI using PIP…
		sed -i -re 's/([^=]+)(==[^=]+)/\1/' requirements.txt
	'';

	nativeCheckInputs = [
		factory-boy
		packaging
		testfixtures
	];

	# Source: https://github.com/modoboa/modoboa-radicale/blob/master/.github/workflows/plugin.yml
	#
	# Upstream tests are run with PostgreSQL or MySQL, not SQLite, so they are
	# not guaranteed to work.
#	checkPhase = ''
#		pushd test_project
#		export DB=SQLITE
#		${python.interpreter} ./manage.py test modoboa_radicale
#		popd
#	'';
	doCheck = false;

	pythonImportsCheck = [ "modoboa_radicale" ];

	meta = with lib; {
		homepage = "https://modoboa-radicale.readthedocs.io/";
		description = "Mail hosting made simple – The Radicale calendar and contacts frontend";
		license = licenses.mit;
		maintainers = with maintainers; [ ];
	};
}
