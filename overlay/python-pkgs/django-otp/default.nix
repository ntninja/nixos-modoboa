{ lib
, buildPythonPackage
, fetchFromGitHub
, python
, hatchling
, django
, freezegun
, pythonOlder
, qrcode
}:

buildPythonPackage rec {
  pname = "django-otp";
  version = "1.3.0";
  pyproject = true;
  disabled = pythonOlder "3";

  src = fetchFromGitHub {
    owner = "django-otp";
    repo = "django-otp";
    rev = "v${version}";
    hash = "sha256-T9sAKAjH8ZbfxYzRKgTDoAlZ/+uLj2R/T/ZHXhtjjVk=";
  };

  nativeBuildInputs = [
    hatchling
  ];

  propagatedBuildInputs = [
    django
    qrcode
  ];

  nativeCheckInputs = [
    django
    freezegun
  ];

  checkPhase = ''
    export PYTHONPATH="test:$PYTHONPATH"
    export DJANGO_SETTINGS_MODULE="test_project.settings"
    ${python.interpreter} -s -m django test django_otp
  '';

  pythonImportsCheck = [ "django_otp" ];

  meta = with lib; {
    homepage = "https://github.com/django-otp/django-otp";
    description = "Pluggable framework for adding two-factor authentication to Django using one-time passwords";
    longDescription = ''
      django-otp makes it easy to add support for one-time passwords (OTPs) to
      Django. It can be integrated at various levels, depending on how much
      customization is required. It integrates with `django.contrib.auth`,
      although it is not a Django authentication backend. The primary target is
      developers wishing to incorporate OTPs into their Django projects as a
      form of two-factor authentication.

      Several simple OTP plugins are included and more are available separately.
      This package also includes an implementation of OATH HOTP and TOTP for
      convenience, as these are standard OTP algorithms used by multiple plugins.
    '';
    license = licenses.unlicense;
    maintainers = with maintainers; [ ];
  };
}
