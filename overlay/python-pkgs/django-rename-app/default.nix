{ lib
, buildPythonPackage
, fetchFromGitHub
, python
, django
, pythonOlder
}:

buildPythonPackage rec {
	pname = "django-rename-app";
	version = "0.1.7";
	disabled = pythonOlder "3";

	src = fetchFromGitHub {
		owner = "odwyersoftware";
		repo = "django-rename-app";
		rev = "${version}";
		hash = "sha256-eFDIPx800pZC6EXBnTzpDcDEV2TeBgX1RW738QXdp8w=";
	};

	nativeBuildInputs = [
	];

	propagatedBuildInputs = [
		django
	];

	# Note: Contrary to what the `setup.cfg` suggests this packages does not
	#       actually have any tests!
	pythonImportsCheck = [ "django_rename_app" ];

	meta = with lib; {
		homepage = "https://github.com/odwyersoftware/django-rename-app";
		description = "A Django Management Command to rename existing Django Applications";
		license = licenses.asl20;
		maintainers = with maintainers; [ ];
	};
}
