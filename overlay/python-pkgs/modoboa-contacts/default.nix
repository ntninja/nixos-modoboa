{ lib
, buildPythonPackage
, fetchPypi
, python
, pythonOlder
# Pre-dependencies
, pip
, setuptools
, setuptools-scm
# Dependencies
, caldav
, django-filter
, django-webpack-loader
, modoboa
, vobject
# Test dependencies
, factory-boy
, httmock
, testfixtures
}:

buildPythonPackage rec {
	pname = "modoboa-contacts";
	version = "1.1.2";
	disabled = pythonOlder "3.8";

	# Note: Source contains pre-built frontend created using WebPack
	src = fetchPypi {
		inherit pname version;
		hash = "sha256-f10Mg60AYQ+kmuiFWIqVAUq3IA3DEghFnSz3Zh5QlQY=";
	};

	nativeBuildInputs = [
		pip  # Used in `setup.py` but not declared
		setuptools
		setuptools-scm
	];

	propagatedBuildInputs = [
		caldav
		django-filter
		django-webpack-loader
		modoboa
		vobject
	];

	preBuild = ''
		# Release has exact version requirements that do not reflect what is
		# actually needed
		#
		# In addition, for all packages listed in requirements.txt that are
		# not present on the system, the setup.py will attempt to auto-install
		# them from PyPI using PIP…
		sed -i -re 's/([^=]+)(==[^=]+)/\1/' requirements.txt
	'';

	nativeCheckInputs = [
		factory-boy
		httmock
		testfixtures
	];

	# Source: https://github.com/modoboa/modoboa-contacts/blob/master/.github/workflows/plugin.yml
	#
	# Upstream tests are run with PostgreSQL or MySQL, not SQLite, so they are
	# not guaranteed to work.
#	checkPhase = ''
#		pushd test_project
#		export DB=SQLITE
#		${python.interpreter} ./manage.py test modoboa_contacts
#		popd
#	'';
	doCheck = false;

	pythonImportsCheck = [ "modoboa_contacts" ];

	meta = with lib; {
		homepage = "https://github.com/modoboa/modoboa-contacts/";
		description = "Mail hosting made simple – Address book plugin";
		license = licenses.mit;
		maintainers = with maintainers; [ ];
	};
}
