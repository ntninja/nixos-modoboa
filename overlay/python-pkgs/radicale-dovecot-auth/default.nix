{ lib
, buildPythonPackage
, fetchFromGitHub
# Dependencies
, radicale
}:

buildPythonPackage rec {
	pname = "radicale-dovecot-auth";
	version = "0.4.1";

	src = fetchFromGitHub {
		owner = "Arvedui";
		repo = "radicale-dovecot-auth";
		rev = "v${version}";
		hash = "sha256-MtqlPffs7/kgUSdA35I2RoRPIQA5zs5YCWBrBSFMAgo=";
	};

	#FIXME: Package is loaded by Radicale as plugin and Radicale is declared
	#       as an app so depending on it causes duplicate package issues when later
	# building the Radicale derivation including extra plugins.
	nativeBuildInputs = [
		radicale
	];

	pythonImportsCheck = [ "radicale_dovecot_auth" ];

	meta = with lib; {
		homepage = "https://github.com/Arvedui/radicale-dovecot-auth";
		description = "Dovecot authentication plugin for Radicale";
		license = licenses.gpl3Plus;
		maintainers = with maintainers; [ ];
	};
}
