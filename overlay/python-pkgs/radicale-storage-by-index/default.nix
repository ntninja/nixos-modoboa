{ lib
, buildPythonPackage
, fetchFromGitHub
# Pre-dependencies
, pip
, pytest-runner
# Dependencies
, radicale
}:

buildPythonPackage rec {
	pname = "radicale-storage-by-index";
	version = "1.0.1+9dc8b6f8";

	src = fetchFromGitHub {
		owner = "tonioo";
		repo = "RadicaleStorageByIndex";
		rev = "9dc8b6f8a8fc69eefcf5d61f0fa385471e75ce02";
		hash = "sha256-7jyV7Ndge1I2Oyx6G5M8m/Vxzr3J1F3EzhGwN+c5SOU=";
	};

	nativeBuildInputs = [
		pip  # Used in `setup.py` but not declared
	#FIXME: Package is loaded by Radicale as plugin and Radicale is declared
	#       as an app so depending on it causes duplicate package issues when
	#       later building the Radicale derivation including extra plugins.
	] ++ [
		radicale
	];

#	propagatedBuildInputs = [
#		radicale
#	];

	patches = [
		./01-disable-pytest-runner-requirement.patch
	];

	#XXX: Tests of package have not been updated for Radicale 3.x and require
	#     the internal `radicale.tests` package which is not part of the
	#     Radicale distribution at all
	doCheck = false;
	pythonImportsCheck = [ "radicale_storage_by_index" ];

	meta = with lib; {
		homepage = "https://github.com/tonioo/RadicaleStorageByIndex";
		description = "Storage extension for Radicale that optimizes reporting using a SQLite index";
		license = licenses.bsd3;  # Only says “BSD licence”, so assume the more restrictive one
		maintainers = with maintainers; [ ];
	};
}
