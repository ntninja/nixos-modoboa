{ lib
, buildPythonPackage
, fetchPypi
, python
, pythonOlder

, pip
, setuptools
, setuptools-scm

, chardet
, lxml_4
, modoboa

, factory-boy
, testfixtures
}:

buildPythonPackage rec {
	pname = "modoboa-webmail";
	version = "1.9.0";
	disabled = pythonOlder "3.8";

	src = fetchPypi {
		inherit pname version;
		hash = "sha256-hGmNWek6/NGBgorpSNoGNSg3B5Xvpl0HA7fHb2EzC54=";
	};

	nativeBuildInputs = [
		pip  # Used in `setup.py` but not declared
		setuptools
		setuptools-scm
	];

	# Source: https://github.com/modoboa/modoboa-webmail/blob/master/requirements.txt
	propagatedBuildInputs = [
		chardet
		lxml_4
		modoboa
	];

	nativeCheckInputs = [
		factory-boy
		testfixtures
	];

	# Source: https://github.com/modoboa/modoboa-webmail/blob/master/.github/workflows/plugin.yml
	#
	# Upstream tests are run with PostgreSQL or MySQL, not SQLite, so they are
	# not guaranteed to work.
#	checkPhase = ''
#		pushd test_project
#		export DB=SQLITE
#		${python.interpreter} ./manage.py test modoboa_webmail
#		popd
#	'';
	doCheck = false;
	pythonImportsCheck = [ "modoboa_webmail" ];

	meta = with lib; {
		homepage = "https://modoboa.org/";
		description = "Webmail extension for Modoboa – Mail hosting made simple";
		license = licenses.mit;
		maintainers = with maintainers; [ ];
	};
}
