{ lib
, buildPythonPackage
, fetchPypi
, python
, pythonOlder

, pip
, setuptools
, setuptools-scm

, modoboa
}:

buildPythonPackage rec {
	pname = "modoboa-rspamd";
	version = "1.0.0";
	pyproject = true;
	disabled = pythonOlder "3.8";

	src = fetchPypi {
		inherit pname version;
		hash = "sha256-2AM6DGTuQbIJkOipN3hedN/jT3le8rAhbb2MuYdy+Ew=";
	};

	nativeBuildInputs = [
		pip  # Used in `setup.py` but not declared
		setuptools
		setuptools-scm
	];

	# Source: https://github.com/modoboa/modoboa-rspamd/blob/master/requirements.txt
	propagatedBuildInputs = [
		modoboa
	];

	# This package does not have any tests
	pythonImportsCheck = [ "modoboa_rspamd" ];

	meta = with lib; {
		homepage = "https://modoboa.org/";
		description = "rspamd frontend for Modoboa – Mail hosting made simple";
		license = licenses.mit;
		maintainers = with maintainers; [ ];
	};
}
