#!/usr/bin/env python
import importlib.util
import json
import locale
import os
import pathlib
import re
import shutil
import subprocess
import sys

PYTHON_EXE = pathlib.Path(sys.argv[1])
SRC_DIR = pathlib.Path(sys.argv[2])
DST_DIR = pathlib.Path(os.environ["out"])

SITE_DATA_ROOT = sys.argv[3]
SITE_SETTINGS_PATH = pathlib.Path(sys.argv[4])

MODOBOA_DIR = pathlib.Path(importlib.util.find_spec('modoboa').origin).parent

def subst_callback(match):
	name = match.group(1)
	if name == "DATA_ROOT":
		return repr(SITE_DATA_ROOT)
	raise ValueError(f"Unknown substitution value “{name}” in `instance/instance/settings.py`")

# Copy everything to target directory ignoring permissions, except for Python
# files outside the Django instance directory, which are always executable and
# have the correct interpreter set
def copyfile(src, dst, *, follow_symlinks=True):
	src = pathlib.Path(src)

	# Perform required substitutions in “settings.py”
	if src == SRC_DIR / "instance" / "settings.py":
		with open(src, "r") as src_file, open(dst, "w") as dst_file:
			dst_file.write(re.sub(r"(?<!`)\{\{([^\}]+)\}\}(?!`)", subst_callback, src_file.read()))

	# Copy executable files with correct shebang and executable bit
	elif src.name.endswith(".py") and src.parent != SRC_DIR / "instance":
		with open(src, "r") as src_file, open(dst, "w") as dst_file:
			dst_file.write(f"#!{PYTHON_EXE}\n")
			shutil.copyfileobj(src_file, dst_file)
		os.chmod(dst, 0o755)

	# Copy everything else as-is
	else:
		shutil.copyfile(src, dst, follow_symlinks=follow_symlinks)
shutil.copytree(SRC_DIR, DST_DIR, copy_function=copyfile)

# Append site settings file contents to Django settings file
with open(DST_DIR / "instance" / "settings.py", "a") as settings_file, \
     open(SITE_SETTINGS_PATH, "r") as site_settings_file:
	shutil.copyfileobj(site_settings_file, settings_file)

# Work around `shutils.copytree` also copying directory permissions
for dirpath, _, _ in os.walk(DST_DIR):
	os.chmod(dirpath, 0o755)

# Symlink `manage.py` in “bin” subdirectory so that the resulting derivation
# can be added to `environment.systemPackages` to conviniently make that
# command available
(DST_DIR / "bin").mkdir(exist_ok=True)
(DST_DIR / "bin" / "modoboa-manage").symlink_to("../manage.py")

MANAGE_CMD = DST_DIR / "manage.py"

# Copy site static files (will hopefully be deduplicated by Nix store)
subprocess.run([MANAGE_CMD, "collectstatic", "--no-input"], check=True)