{ lib
, buildPythonPackage
, fetchPypi
, python
, pythonOlder
, pythonRelaxDepsHook

, setuptools
, setuptools-scm

, argon2-cffi
, aiosmtplib
, asgiref
, bcrypt
, chardet
, cryptography
, defusedxml
, django_4
, django-auth-ldap
, django-ckeditor
, django-cors-headers
, django-environ
, django-filter
, django-oauth-toolkit
, django-otp
, django-phonenumber-field
, django-rename-app
, django-reversion
, django-rq
, django-xforwardedfor-middleware
, djangorestframework
, dj-database-url
, dnspython
, drf-spectacular
, feedparser
, fido2
, gevent
, jsonfield
, lxml_4
, mysqlclient
, oath
, ovh
, passlib
, pillow
, progressbar33
, psycopg
, python-dateutil
, python-magic
, pytz
, qrcode
, redis
, reportlab
, requests
, rq
, rrdtool
, sievelib
, tldextract
, uritemplate

, factory-boy
, httmock
, testfixtures
}:

# While Modoboa is meant to be used as a standalone software package, rather
# than a library, it still needs to be loaded by an external WSGI server to
# be useful, so it still behaves as a python package from Nix’s point-of-view.
buildPythonPackage rec {
	pname = "modoboa";
	version = "2.3.2";
	pyproject = true;
	disabled = pythonOlder "3.8";

	src = fetchPypi {
		inherit pname version;
		hash = "sha256-47bwQoAl9cK+wj8qFkBS8L+BRwkACWvcVNLFOpJeWHE=";
	};

	patches = [
		./01-symlink-frontend-dir-contents.patch
		./02-nohumanize-notification-interval.patch
	];

	nativeBuildInputs = [
		setuptools
		setuptools-scm  # Always includes TOML support nowadays
		pythonRelaxDepsHook
	];

	# Source: https://github.com/modoboa/modoboa/blob/master/requirements.txt
	propagatedBuildInputs = [
		django_4                         #>4.0,<5.0
		django-ckeditor                  #==6.7.1
		django-phonenumber-field         #==8.0.0
	] ++ django-phonenumber-field.optional-dependencies.phonenumbers ++ [
		django-reversion                 #==5.0.12
		django-xforwardedfor-middleware  #==2.0 → missing
		django-otp                       #==1.5.0
		django-filter
		django-rename-app                #==0.1.5 → missing
		django-environ

		dj-database-url

		djangorestframework              #>=3.14,<3.16
		drf-spectacular
		uritemplate
		django-oauth-toolkit
		django-cors-headers

		passlib                          #~=1.7.4
		bcrypt

		asgiref
		dnspython                        #==2.6.1
		feedparser                       #==6.0.11
		fido2                            #==1.1.3
		gevent                           #==24.2.1
		jsonfield                        #==3.1.0
		pillow
		progressbar33                    #==2.4
		python-dateutil
		cryptography
		pytz
		requests
		lxml_4
		chardet
		ovh
		oath
		redis                            #>=4.2.0rc1
		rrdtool                          #>=0.1.10
		qrcode
		
		aiosmtplib

		# PDF credentials
		reportlab

		# DMARC
		tldextract                       #>=2.0.2
		defusedxml                       #>=0.6.0
		python-magic                     #>=0.4.24

		# RQ
		rq                               #>=1.13.0
		django-rq

		# Sieve
		sievelib                         #>=1.4.1
	];

	# Modoboa uses way too many exact version dependencies that actually aren’t
	pythonRelaxDeps = true;

	passthru.optional-dependencies = {
		ldap       = [ django-auth-ldap ];
		mysql      = [ mysqlclient ];
		postgresql = [ psycopg ];
		argon2     = [ argon2-cffi ];
	};

	# Expose derivation that can be used to generate a Modoboa instance
	# directory on-the-fly
	#
	# Based on what the `modoboa-admin.py deploy` command does internally but
	# adapted for Nix.
	passthru.mkInstance = pythonEnv: dataDir: siteSettingsPath: derivation {
		name = "modoboa-instance";
		system = builtins.currentSystem;
		builder = "${pythonEnv}/bin/python";
		args = [ ./mkInstance.py "${pythonEnv}/bin/python" ./instance dataDir siteSettingsPath ];

		# Environment variable that may be used by the provided site-settings
		# file to determine that it is read during derivation build
		MODOBOA_BUILDING_INSTANCE = "1";
	};

	nativeCheckInputs = [
		argon2-cffi
		factory-boy
		httmock
		testfixtures
	] ++ passthru.optional-dependencies.postgresql ++ [
	] ++ passthru.optional-dependencies.mysql;

	# Source: https://github.com/modoboa/modoboa/blob/master/tox.ini
	#
	# FIXME: Most of the test failures are due to dnspython expecting
	#        /etc/resolv.conf, which should probably be fixed upstream.
#	checkPhase = ''
#		pushd test_project
#		export DB=SQLITE
#		# Skipping modoboa.core, modoboa.admin and modoboa.dnstools which have
#		# a bunch of tests that expect either /etc/resolv.conf, network access
#		# or a running Redis instance
#		${python.interpreter} ./manage.py test modoboa.lib modoboa.limits modoboa.transport modoboa.relaydomains nmodoboa.ldapsync
#		popd
#	'';
	pythonImportsCheck = [ "modoboa" ];

	meta = with lib; {
		homepage = "https://modoboa.org/";
		description = "Mail hosting made simple";
		longDescription = ''
			Modoboa is a mail hosting and management platform including a modern
			and simplified Web User Interface. It provides useful components
			such as an administration panel and webmail.
		'';
		license = licenses.isc;
		maintainers = with maintainers; [ ];
	};
}
