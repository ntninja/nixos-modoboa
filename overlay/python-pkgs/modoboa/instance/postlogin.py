# Helper script for Dovecot to update a user’s last-login timestamp

# Note that this is different from the configuration suggested in the Modoboa
# installation manual since the author felt that it would be better to reuse
# the Django database configuration already present in the Modoboa Nix
# environment rather than dependening on the full PostgreSQL and MySQL full
# packages just to have access to the `psql`, `mysql` and `sqlite` utilities.
#
# Background: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#last-login-tracking
import os
import sys

USERNAME = os.environ["USER"]

os.environ["DJANGO_SETTINGS_MODULE"] = "instance.settings"

# Execute user last login query using Django’s database abstraction
from django.db import connection
with connection.cursor() as cursor:
	cursor.execute("UPDATE core_user SET last_login=NOW() WHERE username=%s", (USERNAME,))

# Tell `script-login` that we finished successfully
os.execvp(sys.argv[1], sys.argv[1:])