# Helper to write some selected settings (mostly local filesystem paths) to
# the Modoboa database
import argparse
import pathlib
import os
import sys

BASE_DIR = pathlib.Path(__file__).parent

parser = argparse.ArgumentParser()
parser.add_argument("--dkim-keys-path", nargs=1)
parser.add_argument("--maillog-paths", nargs=2)
parser.add_argument("--pdf-credentials-path", nargs=1)
parser.add_argument("--rspamd-map-paths", nargs=2)
parser.add_argument("--radicale-rights-path", nargs=1)
args = parser.parse_args()

sys.path.insert(0, str(BASE_DIR.parent))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "instance.settings")

# Bootstrap Django applications
import django
django.setup()

# Load Modoboa local config parameters into memory
from modoboa.core import models
localconfig = models.LocalConfig.objects.get()
parameters  = localconfig._parameters

if args.dkim_keys_path:
	parameters.setdefault("admin", {}).update({
		"dkim_keys_storage_dir": args.dkim_keys_path[0],
	})

if args.maillog_paths:
	parameters.setdefault("maillog", {}).update({
		"logfile": args.maillog_paths[0],
		"rrd_rootdir": args.maillog_paths[1],
	})

if args.pdf_credentials_path:
	parameters.setdefault("pdfcredentials", {}).update({
		"storage_dir": args.pdf_credentials_path[0],
	})

if args.rspamd_map_paths:
	parameters.setdefault("modoboa_rspamd", {}).update({
		"key_map_path": args.rspamd_map_paths[0],
		"selector_map_path": args.rspamd_map_paths[1],
	})

if args.radicale_rights_path:
	parameters.setdefault("modoboa_radicale", {}).update({
		"rights_file_path": args.radicale_rights_path[0],
	})

# Save potentially modified local config parameters to database
localconfig._parameters = parameters
localconfig.save()