import contextlib
import multiprocessing
import pathlib
import os
import subprocess
import sys
import typing as ty

BASE_DIR = pathlib.Path(__file__).parent


## HELPERS
#
# Only call these from the subprocess to ensure re-evaluation of the Django
# environment between migration runs!

_connection = None
def get_connection():
	"""
	Returns the Django database connection object after initializing the Django
	environment
	"""
	global _connection

	if _connection is None:
		# Django environment is only initialized here
		sys.path.insert(0, str(BASE_DIR.parent))
		os.environ.setdefault("DJANGO_SETTINGS_MODULE", "instance.settings")

		from django.db import connection
		_connection = connection

	return _connection

def check_have_migration(cursor, app: str, name: str) -> bool:
	"""
	Queries the migration table to check if the given migration has previously
	been applied by Django

	Useful to to run code only below a certain upgrade level, by checking for
	a migration only added after the target version.
	"""
	from django.db import ProgrammingError

	try:
		# Note: Django always escapes queries using %s – even on SQLite!
		# Source: https://docs.djangoproject.com/en/5.1/topics/db/sql/#connections-and-cursors
		cursor.execute("SELECT COUNT(*) FROM django_migrations WHERE app=%s AND name=%s", (app, name,))
	except ProgrammingError:
		# Likely means migration tables is absent on a freshly created
		# Modoboa database – WE DO NOT NEED TO PERFORM SPECIAL MIGRATION STEPS
		# IN THIS CASE, so just always return True!
		return True
	return cursor.fetchone()[0] > 0

def update_app_name(cursor, old_name: str, new_name: str) -> None:
	"""
	Renames a Modoboa app (cannot be done by regular Django migration)
	"""
	cursor.execute("UPDATE django_content_type SET app_label=%s WHERE app_label=%s", (new_name, old_name))
	cursor.execute("UPDATE django_migrations SET app=%s WHERE app=%s", (new_name, old_name))

def run_manage(*args: str, **kwargs) -> None:
	"""
	Runs manage.py with the given `args` and ensure it finished successfully
	"""
	import django
	django.setup()
	
	from django.core.management import call_command
	call_command(*args, **kwargs)

def get_setting(name: str) -> ty.Any:
	from django.conf import settings
	return getattr(settings, name)

def restart() -> ty.NoReturn:
	"""
	Requests new clean migration run by stopping the migration runner with a
	special exit code 69 (EX_UNAVAILABLE)
	"""
	sys.exit(69)


## MIGRATION LOGIC

class MigrationRunner(multiprocessing.Process):
	def run(self):
		connection = get_connection()
		should_rerun = False

		with connection.cursor() as cursor:
			# Upgrade steps to Modoboa 2.3.0 (skipped on fresh installs)
			if not check_have_migration(cursor, "core", "0027_alter_user_language"):
				# Modoboa Postfix Autoreply extension was merged into core
				update_app_name(cursor, "modoboa_postfix_autoreply", "postfix_autoreply")

				# Done performing changes from this migration runner
				cursor.close()
				connection.commit()

				# Apply Django migrations up to 2.3.0
				#
				# Source: `git diff --name-status --diff-filter=ADR 2.2.4..2.3.0 | grep /migrations/`
				run_manage("migrate", "--no-input", "core", "0029_rename_tfa_enabled_user_totp_enabled_and_more")
				run_manage("migrate", "--no-input", "admin", "0023_auto_20240320_1037")

				# Request another migration run with updated database
				restart()

		# Perform regular migration run
		run_manage("migrate", "--no-input")

		# Add initial data from newly installed extensions
		#
		# Technically, this may re-add data that was intentionally removed by
		# the administrator from the database. In practice however, this
		# appears to only be used for basic group permissions that could only
		# be removed by directly editing the database (and causing “weird”
		# issues when absent) so the “just works” effect of having this always
		# enabled should outweigh that potential issue encountered for that
		# advanced use-case.
		#
		# This also copies the data for the new web frontend.
		run_manage("load_initial_data")

		# Fix up permissions of copied frontend files so that web servers can read
		frontend_dir = pathlib.Path(get_setting("DATA_DIR")) / "frontend"
		os.chmod(frontend_dir, 0o755)
		for root, dirnames, filenames in os.walk(frontend_dir, followlinks=False):
			# Change mode of discovered files to make them world readable,
			# without following symlinks
			#
			# Suppressing `NotImplementedError` is required, since Linux
			# doesn’t allow changing the mode of symlinks, but using `.lchmod`
			# still ensures that encountered symlinks are not followed.
			root = pathlib.Path(root)
			for name in dirnames:
				with contextlib.suppress(NotImplementedError):
					(root / name).lchmod(0o755)
			for name in filenames:
				with contextlib.suppress(NotImplementedError):
					(root / name).lchmod(0o644)

		# Add database workarounds detailed in the installation manual to make
		# OpenDKIM integration and quotas work in case they are used
		subprocess.run([str(BASE_DIR / "apply-database-workarounds.py")], check=True)

		# Exit after last migration run
		sys.exit(0)


def main():
	while True:
		process = MigrationRunner()
		process.start()
		process.join()
		if process.exitcode != 69:
			return process.exitcode

if __name__ == "__main__":
	sys.exit(main())