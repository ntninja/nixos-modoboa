# One-time initialization helper to write secret and OIDC key to known path
#
# The key generation function is the same as used by `modoboa-admin.py deploy`.
import pathlib
import secrets
import string
import sys

import modoboa.core.utils

DATA_DIR = pathlib.Path(sys.argv[1])

# Generate secret key file
key_file_path = DATA_DIR / "secret-key.txt"
key_length    = 60
if not key_file_path.exists():
	c = string.ascii_letters + string.digits + string.punctuation
	secret_key = ''.join(secrets.choice(c) for i in range(key_length))
	key_file_path.write_text(secret_key)

# Generate OIDC private key
modoboa.core.utils.generate_rsa_private_key(DATA_DIR)