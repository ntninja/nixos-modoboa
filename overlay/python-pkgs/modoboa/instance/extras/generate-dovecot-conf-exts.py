import json
import os
import pathlib
import shlex
import sys
import typing as ty

BASE_DIR = pathlib.Path(__file__).parent
DST_DIR = pathlib.Path(sys.argv[1])
DATA_DIR = pathlib.Path(sys.argv[2])  # used to persist OAuth2 client secrets
HOME_PATH_PAT = sys.argv[3]

sys.path.insert(0, str(BASE_DIR.parent))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "instance.settings")

def getitem_tolerant(obj: ty.Dict[str, object], key: str) -> ty.Optional[object]:
	value = obj.get(key)
	return value if value else None

def remove_indent(indent: str, s: str) -> str:
	"""
	Remove the given indentation `indent` from the start of each line in `s` and
	return the new string
	"""
	return "\n".join(
		line.removeprefix(indent)
		for line in s.split("\n")
	)

def escape_sql_str(s: str) -> str:
	"""
	Escape the given so that it may safely be embedded inside a SQL statement
	"""
	return "'" + s.replace("'", "''") + "'"

def escape_dovecot_str(s: str) -> str:
	"""
	Escape Dovecot string value
	
	Makes it safe to embed an arbitrary multi-line string in a Dovecot
	configuration file.
	"""
	# Apply escaping to string contents
	s = s.replace("\\", "\\\\").replace("\"", "\\\"").replace("$", "\\$")
	
	# Add continuation-line character to allow string to stretch over multiple
	# lines
	#
	# Using `.rstrip()` is required since Dovecot doesn’t line it if the final
	# line in a multi-line configuration value is empty. The `.lstrip()` just
	# makes the first line start with `"\` instead of `" \` (cosmetic).
	s = " \\\n\t".join(s.rstrip().split("\n")).lstrip()
	
	# Surround content by quotation marks
	return f"\"{s}\""

def escape_dovecot_str_with_vars(s: str) -> str:
	"""
	Like `escape_dovecot_str` but also escapes any config vars (%…) in the string
	"""
	return escape_dovecot_str(s.replace("%", "%%"))

def make_mysql_connect_string(**attrs: ty.Optional[str]) -> str:
	"""
	Build a Dovecot MySQL driver compatible connect string for the given
	database settings
	
	Note: As of 2023 the Dovecot MYSQL driver does not support any form of
	      escaping for values, instead it just splits the connection string at
	      each space and then looks at the prefix of the given part to determine
	      the type of each assignment.
	"""
	return " ".join(
		f"{name}={value}"
		for name, value in attrs.items()
		if value is not None
	)

def make_libpq_connect_string(**attrs: ty.Optional[str]) -> str:
	"""
	Build a libpg compatible connect string (as used by Dovecot) for the given
	database settings
	"""
	def escape(s: str) -> str:
		return "'" + s.replace("'", "\\'") + "'"
	
	return " ".join(
		f"{name}={escape(value)}"
		for name, value in attrs.items()
		if value is not None
	)


# Query Modoboa configuration for database settings
from django.conf import settings
from django.utils import timezone
db_settings = settings.DATABASES["default"]  # Must exist for Modoboa itself

import certifi  # Reveals CA certificate bundle path used by Modoboa elsewhere
import urllib3.util  # Has saner URL-build than stdlib

date = str(timezone.now())
commandline = shlex.join([pathlib.Path(sys.argv[0]).name, *sys.argv[1:]])

if db_settings["ENGINE"] == "django.db.backends.mysql":
	connect_driver = "mysql"
	
	connect_string = make_mysql_connect_string(
		host     = db_settings["HOST"],
		port     = getitem_tolerant(db_settings, "PORT"),
		dbname   = db_settings["NAME"],
		user     = db_settings["USER"],
		password = getitem_tolerant(db_settings, "PASSWORD"),
	)
	
	sql_user_query = remove_indent("\t\t", f"""
		SELECT {escape_sql_str(HOME_PATH_PAT)} AS home,
		       CONCAT('*:bytes=', mb.quota, 'M') AS quota_rule
		FROM admin_mailbox mb
		INNER JOIN admin_domain dom ON mb.domain_id=dom.id
		INNER JOIN core_user u ON u.id=mb.user_id
		WHERE (mb.is_send_only=0 OR '%s' NOT IN ('imap', 'pop3', 'lmtp'))
		AND mb.address='%n' AND dom.name='%d'
	""")
	
	sql_password_query = remove_indent("\t\t", f"""
		SELECT email AS user, password, {escape_sql_str(HOME_PATH_PAT)} AS userdb_home,
		       CONCAT('*:bytes=', mb.quota, 'M') AS userdb_quota_rule
		FROM core_user u
		INNER JOIN admin_mailbox mb ON u.id=mb.user_id
		INNER JOIN admin_domain dom ON mb.domain_id=dom.id
		WHERE (mb.is_send_only=0 OR '%s' NOT IN ('imap', 'pop3'))
		AND u.email='%u' AND u.is_active=1 AND dom.enabled=1
	""")
	
	sql_iterate_query = "SELECT email AS user FROM core_user WHERE is_active"
elif db_settings["ENGINE"] == "django.db.backends.postgresql":
	connect_driver = "pgsql"
	
	connect_string = make_libpq_connect_string(
		host     = db_settings["HOST"],
		port     = getitem_tolerant(db_settings, "PORT"),
		dbname   = db_settings["NAME"],
		user     = db_settings["USER"],
		password = getitem_tolerant(db_settings, "PASSWORD"),
	)
	
	sql_user_query = remove_indent("\t\t", f"""
		SELECT {escape_sql_str(HOME_PATH_PAT)} AS home,
		       '*:bytes=' || mb.quota || 'M' AS quota_rule
		FROM admin_mailbox mb
		INNER JOIN admin_domain dom ON mb.domain_id=dom.id
		INNER JOIN core_user u ON u.id=mb.user_id
		WHERE (mb.is_send_only IS NOT TRUE OR '%s' NOT IN ('imap', 'pop3', 'lmtp'))
		AND mb.address='%n' AND dom.name='%d'
	""")
	
	sql_password_query = remove_indent("\t\t", f"""
		SELECT email AS user, password, {escape_sql_str(HOME_PATH_PAT)} AS userdb_home,
		       '*:bytes=' || mb.quota || 'M' AS userdb_quota_rule
		FROM core_user u
		INNER JOIN admin_mailbox mb ON u.id=mb.user_id
		INNER JOIN admin_domain dom ON mb.domain_id=dom.id
		WHERE (mb.is_send_only IS NOT TRUE OR '%s' NOT IN ('imap', 'pop3'))
		AND u.email='%u' AND u.is_active AND dom.enabled
	""")
	
	sql_iterate_query = "SELECT email AS user FROM core_user WHERE is_active"
elif db_settings["ENGINE"] == "django.db.backends.sqlite3":
	connect_driver = "sqlite"
	
	# Note: The Dovecot SQlite driver does not support any form of escaping,
	#       it just splits the string at each space and then checks if it
	#       contains an equal sign to determine whether it is a path or parameter
	connect_string = db_settings["NAME"]
	
	sql_user_query = remove_indent("\t\t", f"""
		SELECT {escape_sql_str(HOME_PATH_PAT)} AS home,
		       ('*:bytes=' || mb.quota || 'M') AS quota_rule
		FROM admin_mailbox mb
		INNER JOIN admin_domain dom ON mb.domain_id=dom.id
		INNER JOIN core_user u ON u.id=mb.user_id
		WHERE (mb.is_send_only=0 OR '%s' NOT IN ('imap', 'pop3', 'lmtp'))
		AND mb.address='%n' AND dom.name='%d'
	""")
	
	sql_password_query = remove_indent("\t\t", f"""
		SELECT email AS user, password, {escape_sql_str(HOME_PATH_PAT)} AS userdb_home,
		       ('*:bytes=' || mb.quota || 'M') AS userdb_quota_rule
		FROM core_user u
		INNER JOIN admin_mailbox mb ON u.id=mb.user_id
		INNER JOIN admin_domain dom ON mb.domain_id=dom.id
		WHERE (mb.is_send_only=0 OR '%s' NOT IN ('imap', 'pop3'))
		AND u.email='%u' AND u.is_active=1 AND dom.enabled=1
	""")
	
	sql_iterate_query = "SELECT email AS user FROM core_user WHERE is_active"
else:
	raise AssertionError("Django DATABASES[\"default\"][\"ENGINE\"] must be "
	                     "one of “django.db.backends.mysql”, "
	                     "“django.db.backends.postgresql” or "
	                     "“django.db.backends.sqlite3”")

DST_DIR.mkdir(exist_ok=True)
DATA_DIR.mkdir(exist_ok=True)

try:
	# Attempt to reuse pre-registered client secret
	oauth2_client_secret = (DATA_DIR / "oauth2-client-secret.txt").read_text()
except FileNotFoundError:
	# Register new client secret in database
	#
	# Corresponds to the following CLI command:
	# `manage.py createapplication --name=Dovecot --skip-authorization --client-id=dovecot confidential client-credentials`
	#
	# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#oauth-2-authentication
	# Source: https://github.com/jazzband/django-oauth-toolkit/blob/907d70f08c1bef94a485bde8fd3edb51952aec03/oauth2_provider/management/commands/createapplication.py
	import django
	django.setup()
	
	from oauth2_provider.models import get_application_model
	
	Application = get_application_model()
	
	new_application = Application(
		name = "Dovecot",
		skip_authorization = True,
		client_id = "dovecot",
		client_type = "confidential",
		authorization_grant_type = "client-credentials",
	)
	new_application.full_clean()
	
	oauth2_client_secret = new_application.client_secret
	new_application.save()
	
	# Save newly registered client secret for future use
	(DATA_DIR / "oauth2-client-secret.txt").write_text(oauth2_client_secret)

oauth2_introspect_url = urllib3.util.Url(
	scheme = "https",
	auth = f"dovecot:{oauth2_client_secret}",
	host = settings.ALLOWED_HOSTS[0],  # Hopefully also reachable internally -.-
	path = "/api/o/introspect/",
).url

# Write Dovecot configuration for querying user information from the database
#
# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#authentication
with open(DST_DIR / "dovecot-sql.conf.ext", "w") as sql_conf_file:
	sql_conf_file.write(remove_indent("\t\t", f"""\
		# This file was generated on {date} by running:
		# {commandline}
		# DO NOT EDIT!
		
		driver = {escape_dovecot_str_with_vars(connect_driver)}
		connect = {escape_dovecot_str_with_vars(connect_string)}
		default_pass_scheme = CRYPT
		
		user_query = {escape_dovecot_str(sql_user_query)}
		password_query = {escape_dovecot_str(sql_password_query)}
		iterate_query = {escape_dovecot_str(sql_iterate_query)}
	""".rstrip("\t")))

# Write Dovecot configuration for accepting OAuth2 credentials for login
#
# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#dovecot-oauth2
with open(DST_DIR / "dovecot-oauth2.conf.ext", "w") as oauth2_conf_file:
	oauth2_conf_file.write(remove_indent("\t\t", f"""\
		# This file was generated on {date} by running:
		# {commandline}
		# DO NOT EDIT!
		
		introspection_mode = post
		introspection_url = {escape_dovecot_str(oauth2_introspect_url)}
		username_attribute = username
		tls_ca_cert_file = {escape_dovecot_str(certifi.where())}
		active_attribute = active
		active_value = true
	""".rstrip("\t")))

# Write Dovecot configuration for querying user quota information from database
#
# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#quota
with open(DST_DIR / "dovecot-dict-sql.conf.ext", "w") as dict_sql_conf_file:
	dict_sql_conf_file.write(remove_indent("\t\t", f"""\
		# This file was generated on {date} by running:
		# {commandline}
		# DO NOT EDIT!
		
		# Note: Database driver ({connect_driver}) needs to be specified in
		#       `dict {{ quota = … }}` block of the main Dovecot configuation
		
		connect = {escape_dovecot_str_with_vars(connect_string)}
		
		map {{
			pattern = priv/quota/storage
			table = admin_quota
			username_field = username
			value_field = bytes
		}}
		
		map {{
			pattern = priv/quota/messages
			table = admin_quota
			username_field = username
			value_field = messages
		}}
	""".rstrip("\t")))