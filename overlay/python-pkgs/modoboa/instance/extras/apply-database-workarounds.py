import pathlib
import os
import sys

BASE_DIR = pathlib.Path(__file__).parent

sys.path.insert(0, str(BASE_DIR.parent))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "instance.settings")

from django.db import connection

# Workarounds for PostgreSQL
#
# Technically these are only required when using quotas but applying them
# doesn’t hurt if the quota support then isn’t used. All of these are
# idempotent so we can safely run them every time Modoboa is started.
#
# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#id2
if connection.vendor == "postgresql":
	with connection.cursor() as cursor:
		cursor.execute("ALTER TABLE admin_quota ALTER COLUMN bytes SET DEFAULT 0;")
		cursor.execute("ALTER TABLE admin_quota ALTER COLUMN messages SET DEFAULT 0;")

		with open(BASE_DIR / "modoboa_postgres_trigger.sql", "r") as merge_quota_trigger_file:
			cursor.execute(merge_quota_trigger_file.read())

# Auxillary view for OpenDKIM
#
# Again, technically this is only needed when using OpenDKIM for DKIM signing
# but it doesn’t hurt to add it in all cases.
#
# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/opendkim.html#database
with connection.cursor() as cursor:
	if connection.vendor == "mysql":
		cursor.execute("""
			CREATE OR REPLACE VIEW dkim AS (
				SELECT id, name as domain_name,
				       dkim_private_key_path AS private_key_path,
				       dkim_key_selector AS selector
				FROM admin_domain WHERE enable_dkim=1
			);
		""")
	elif connection.vendor == "postgresql":
		cursor.execute("""
			CREATE OR REPLACE VIEW dkim AS (
				SELECT id, name as domain_name,
				       dkim_private_key_path AS private_key_path,
				       dkim_key_selector AS selector
				FROM admin_domain WHERE enable_dkim
			);
		""")
	elif connection.vendor == "sqlite3":
		#XXX: Upstream doesn’t actually mention how to do this…
		#
		# Note that we need two statements here, since SQLite doesn’t support
		# `CREATE OR REPLACE VIEW` so it has to be emulated using an extra
		# `DROP VIEW IF EXISTS` statement before the `CREATE VIEW`. This isn’t
		# racey since performing any write operation exclusively locks the
		# database until the data is `COMMIT`ed.
		cursor.execute("DROP VIEW IF EXISTS dkim;")
		cursor.execute("""
			CREATE VIEW dkim AS (
				SELECT id, name as domain_name,
				       dkim_private_key_path AS private_key_path,
				       dkim_key_selector AS selector
				FROM admin_domain WHERE enable_dkim=1
			);
		""")