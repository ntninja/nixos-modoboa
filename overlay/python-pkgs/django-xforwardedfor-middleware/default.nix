{ lib
, buildPythonPackage
, fetchFromGitHub
, django
}:

buildPythonPackage rec {
	pname = "django-xforwardedfor-middleware";
	version = "2.0";

	src = fetchFromGitHub {
		owner = "allo-";
		repo = "django-xforwardedfor-middleware";
		rev = "v${version}";
		hash = "sha256-dDXSb17kXOSeIgY6wid1QFHhUjrapasWgCEb/El51eA=";
	};

	#nativeBuildInputs = [
	#	hatchling
	#];

	propagatedBuildInputs = [
		django
	];

	pythonImportsCheck = [ "x_forwarded_for" ];

	meta = with lib; {
		homepage = "https://github.com/allo-/django-xforwardedfor-middleware";
		description = "Django X-Forwarded-For Middleware – Use the X-Forwarded-For header to get the real ip of a request";
		#license = licenses.bsd2;
		maintainers = with maintainers; [ ];
	};
}
