final: prev:

{
	# Enable Postfix database integrations
	#
	# Shouldn’t be here but the NixOS configuration is missing the `package`
	# option to change it when enabling the service.
	postfix = prev.postfix.override {
		withMySQL = true;
		withPgSQL = true;
		withSQLite = true;
	};

	# Enable Dovecot database integrations
	#
	# Shouldn’t be here but the NixOS configuration is missing the `package`
	# option to change it when enabling the service.
	dovecot = prev.dovecot.override {
		withMySQL = true;
		withPgSQL = true;
		withSQLite = true;
	};
	
	# Python packages
	#
	# Source: https://nixos.wiki/wiki/Overlays#Python_Packages_Overlay
	pythonPackagesExtensions = prev.pythonPackagesExtensions ++ [
		(let
			pkgsPrev = prev;
		in final: prev:
			let
				# Helper: Invoke arbitrary Python package definition using
				#         system-provided versions of all packages as dependencies
				#
				# By default, referencing any package from a different NixPkgs
				# version will (successfully) attempt to satisfy all the
				# dependencies of that NixPkgs versions as well. And while doing
				# so generally works (unless it happens to break dynamic linking
				# for native modules during runtime) it will attempt to build
				# *a lot* of things.
				withPyPrevPkgs = pkg:
					pkg.override (oldArgs:
						oldArgs //
						builtins.intersectAttrs oldArgs pkgsPrev //
						builtins.intersectAttrs oldArgs prev
					);

				# Helper: An implementation of (Python) prev.callPackage to
				#         work around the fact that prev.callPackage appears
				#         to be missing sometimes
				prev_callPackage = path: attrs:
					prev.lib.callPackageWith (pkgsPrev // prev) path attrs;
			in rec {
				# Radicale plugins
				radicale-dovecot-auth = prev_callPackage ./python-pkgs/radicale-dovecot-auth/default.nix {};
				radicale-storage-by-index = prev_callPackage ./python-pkgs/radicale-storage-by-index/default.nix {};

				# Modoboa dependencies: Locally maintained packages
				lxml_4 = prev_callPackage ./python-pkgs/lxml_4/default.nix {};
				caldav = prev.caldav.override {
					lxml = lxml_4;
				};
				
				# Locally maintained packages
				modoboa = prev_callPackage ./python-pkgs/modoboa/default.nix {
					# Modoboa dependencies: Locally maintained packages
					inherit lxml_4;
					
					django-otp = prev_callPackage ./python-pkgs/django-otp/default.nix {};
					django-rename-app = prev_callPackage ./python-pkgs/django-rename-app/default.nix {};
					django-xforwardedfor-middleware = prev_callPackage ./python-pkgs/django-xforwardedfor-middleware/default.nix {};
					rrdtool = prev_callPackage ./python-pkgs/rrdtool/default.nix {};
				};
				modoboa-contacts = prev_callPackage ./python-pkgs/modoboa-contacts/default.nix {
					inherit modoboa caldav;
				};
				modoboa-radicale = prev_callPackage ./python-pkgs/modoboa-radicale/default.nix {
					inherit modoboa caldav;
				};
				modoboa-rspamd = prev_callPackage ./python-pkgs/modoboa-rspamd/default.nix {
					inherit modoboa;
				};
				modoboa-webmail = prev_callPackage ./python-pkgs/modoboa-webmail/default.nix {
					inherit modoboa lxml_4;
				};
			}
		)
	];
}