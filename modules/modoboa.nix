{ config, pkgs, lib, utils, ... }:

#TODO:
# * Find a way to not require Dovecot and Postfix to be overlayed with options
#   to enable the required database backends
# * Radicale as declared on NixOS does not natively support the loading of
#   plugins since its declared a Python application rather than as package
# * ${instanceDir}/extras/apply-database-workarounds.py should really be a
#   Modoboa-internal RunSQL migration…
# * Submit patch to Modoboa to use `settings.BASE_DIR` directly in code
#   (which assumes that runtime data and instance directories are the same)
#   but rather split it into more fine-grained values
# * Upstream the `01-symlink-frontend-dir-contents.patch` patch
# * Modoboa policy service should be served using a Unix domain socket rather
#   than loopback TCP
# * Tests on the Modoboa Python packages fail since they expect dnspython can
#   access /etc/resolv.conf and a running Redis

let
	cfg = config.services.modoboa;
	dovecotHelpers = config.services.dovecot2.helpers;

	runtimeDir = "/run/modoboa";
	dataDir = "/var/lib/modoboa";
	mailDir = "/var/spool/mail";
	maillogFile = "/var/log/mail.log";
in {
	options.services.modoboa = 
	let
		mkEnableOption = description:
			lib.options.mkOption {
				type = lib.types.bool;
				default = false;
				description = description;
				example = true;
			};

		mkDisableOption = description:
			lib.options.mkOption {
				type = lib.types.bool;
				default = true;
				description = description;
				example = false;
			};
	in {
		enable = lib.options.mkEnableOption "the Modoboa mail hosting service";
		
		settings = lib.options.mkOption {
			type = lib.types.submodule {
				freeformType = (pkgs.formats.json {}).type;

				options.ALLOWED_HOSTS = lib.options.mkOption {
					type = lib.types.addCheck (lib.types.listOf lib.types.str) (value: value != []);
					default = [];
					description = ''
						List of allowed hostnames for accessing the Modoboa
						web interface over HTTP.

						At least one value is required if Modoboa is enabled.
					'';
					example = [
						"mail.example.org"
					];
				};

				options.DATABASES =
				let driverDescription = ''
					The name of the Django database engine to use:
					\"django.db.backends.mysql\" (MySQL or MariaDB),
					\"django.db.backends.postgresql\" (PostgreSQL) or
					\"django.db.backends.sqlite3\" (SQLite3).
				'';
				in lib.options.mkOption {
					type = lib.types.attrsOf (lib.types.either (
						lib.types.submodule {
							freeformType = (pkgs.formats.json {}).type;
							
							options.ENGINE = lib.options.mkOption {
								type = lib.types.enum [
									"django.db.backends.mysql"
									"django.db.backends.postgresql"
								];
								description = driverDescription;
							};

							options.HOST = lib.options.mkOption {
								type = lib.types.nonEmptyStr;
								default = "localhost";
								description = ''
									The TCP hostname or Unix domain socket path
									of the database server to connect to.

									Note that both MySQL and PostgreSQL will
									attempt to establish the connection using
									a local Unix domain socket at a well-known
									path if the hostname is set to
									`"localhost"` instead of using TCP.
								'';
								example = "/run/postgresql/.s.PGSQL.5432";
							};

							options.PORT = lib.options.mkOption {
								type = lib.types.nullOr lib.types.port;
								default = null;
								description = ''
									The TCP port number of the database server
									to connect to (ignored if `HOST` specifies
									a Unix domain socket path).
								'';
								
								# Modoboa code breaks if the port number is
								# an integer rather than a string
								apply = value: if value != null then toString value else null;
							};

							options.NAME = lib.options.mkOption {
								type = lib.types.nonEmptyStr;
								description = ''
									Name of an existing database at the
									database server to connect to.
								'';
								example = "modoboa";
							};

							options.USER = lib.options.mkOption {
								type = lib.types.nullOr lib.types.nonEmptyStr;
								default = null;
								description = ''
									Name of an existing database user at the
									database server to connect to.

									The user should be the owner of the database
									specified in `NAME` to ensure that Modoboa
									is able to automatically initialize the
									database and apply migrations.

									For instance with PostgreSQL, the database
									and user may be created like this from the
									command-line (after enabling
									{option}`services.postgresql.enable`):

									```sh
									createuser --pwprompt modoboa
									createdb --owner=modoboa modoboa
									```
								'';
								example = "modoboa";
							};

							options.PASSWORD_FILE = lib.options.mkOption {
								type = lib.types.nullOr (lib.types.either lib.types.path lib.types.str);
								default = null;
								description = ''
									Path to a file containing the password for
									the database user.

									By default, no password is used. Specifying
									a path to a database file instead of
									directly setting the password is a NixOS
									specific extension.
								'';
								example = lib.options.literalExpression "/etc/modoboa/database.passwd";

								# Prevent paths from being copied to the Nix
								# store on JSON conversion
								apply = value: toString value;
							};
						}
					) (
						lib.types.submodule {
							freeformType = (pkgs.formats.json {}).type;
							
							options.ENGINE = lib.options.mkOption {
								type = lib.types.enum [
									"django.db.backends.sqlite3"
								];
								description = driverDescription;
							};

							options.NAME = lib.options.mkOption {
								type = lib.types.path;
								description = ''
									Absolute path to the SQLite3 database file
									to use.

									The file must belong to the Modboa system
									user ({option}`services.modoboa.user`) and
									group ({option}`services.modoboa.group`)
									and be user writable and group readable.
								'';
								default = "/var/lib/modoboa/database.db";
							};
						}
					));
					default = {};

					# Add some settings tweaks recommended by upstream
					#
					# Also remove all items defaulting to `null` since Modoboa
					# can’t handle that well.
					apply = databases: lib.attrsets.mapAttrs (conn: database:
						lib.attrsets.filterAttrs (name: value: value != null) ({
							ATOMIC_REQUESTS = true;
						} // (lib.optionalAttrs (database.ENGINE == "django.db.backends.mysql") {
							OPTIONS = {
								"init_command" = "SET foreign_key_checks = 0;";
							};
						}) // database)
					) databases;
				};

				options.LANGUAGE_CODE = lib.options.mkOption {
					type = lib.types.str;
					default = builtins.head (builtins.split "[._-]" config.i18n.defaultLocale);
					defaultText = lib.literalExpression "i18n.defaultLocale";
					description = ''
						Default language and locale settings used when
						accessing the Modoboa web interface.
					'';
					example = "de";
				};
				
				options.TIME_ZONE = lib.options.mkOption {
					type = lib.types.str;
					default = config.time.timeZone;
					defaultText = lib.literalExpression "time.timeZone";
					description = ''
						Default time zone used when accessing the Modoboa web
						interface.
					'';
					example = "Europe/Vienna";
				};

				options.OTP_TOTP_ISSUER = lib.options.mkOption {
					type = lib.types.str;
					default = builtins.head cfg.settings.ALLOWED_HOSTS;
					defaultText = lib.literalMD "ALLOWED_HOSTS[0]";
					description = ''
						Issuer name displayed to users in their authenticator
						app when enabling two-factor authentication for their
						account.
					'';
					example = "ACME corp mail";
				};

				options.EXTRA_MODOBOA_APPS = lib.options.mkOption {
					type = lib.types.listOf lib.types.str;
					default = [];
					description = ''
						List of module names to append to both the Modoboa
						`MODOBOA_APPS` and the Django `INSTALLED_APPS` list.

						Packages mentioned in this list will have their URL
						patterns automatically added to the root URL pattern
						list by Modoboa core. Additionally, any package
						mentioned in this list that has an
						`<package-name>.settings` module containing an `apply`
						function, will have that function automatically be
						called from the generated `settings.py` to allow it to
						dynamically change the Modoboa global settings.
					'';
				};

				options.EXTRA_INSTALLED_APPS = lib.options.mkOption {
					type = lib.types.listOf lib.types.str;
					default = [];
					description = ''
						List of module names to append to the Django
						`INSTALLED_APPS` list only. For extensions you probably
						want to use {option}`services.modoboa.extensions` or
						{option}`services.modoboa.settings.EXTRA_MODOBOA_APPS`
						instead.
					'';
				};
			};
			default = {};
			description = "Settings to add to the Modoboa Django settings file.";
			example = {
				ALLOWED_HOSTS = [
					"mail.example.org"
				];
			};
		};

		extraConfig = lib.options.mkOption {
			type = lib.types.lines;
			default = "";
			description = ''
				Extra configuration (arbitrary Python code) to appended to the
				Modoboa Django settings file.
			'';
		};
		
		dmarcAddress = lib.options.mkOption {
			type = lib.types.nullOr lib.types.nonEmptyStr;
			default = null;
			description = ''
				E-Mail address to access DMARC reports on.

				The email address must not be used otherwise by Modoboa
				(in particular, it must not be of any user account or alias
				created inside Modoboa) and be specified in the `rua=` field
				of the DMARC record on each domain that remote mail systems
				should produce reports for.
			'';
		};
		
		caddy = lib.options.mkOption {
			type = lib.types.submodule {
				options.enable = mkEnableOption ''
					Whether to configure Caddy as a reverse-proxy for the
					Modoboa web interface.
				'';

				options.hostNames = lib.options.mkOption {
					type = lib.types.addCheck (lib.types.nullOr (lib.types.listOf lib.types.str)) (value: value == null || value != []);
					default = null;
					defaultText = lib.literalExpression "services.modoboa.settings.ALLOWED_HOSTS";
					description = ''
						List of hostnames to configure Caddy to expose the
						Modoboa web interface at.
					'';
					example = [
						"mail.example.org"
					];
				};

				options.exposeRadicale = mkDisableOption ''
					Whether to expose Radicale (if it is also enabled) on the
					same host names that Modoboa is exposed on.
				'';
			};
			default = {};
		};

		dovecot = lib.options.mkOption {
			type = lib.types.submodule {
				options.enable = mkDisableOption ''
					Whether to configure Dovecot with Modoboa-related settings.

					Without this Modoboa will probably not be very useful.
				'';

				options.enableLastLogin = mkDisableOption ''
					Whether to enable last-login tracking for users in Dovecot.
					
					Without this user’s last-login time in the web interface
					will only change when logging into the web interface, not
					when logging in via IMAP/POP3/ManageSieve.
				'';

				options.enableSieve = mkDisableOption ''
					Whether to enable mail filtering (Sieve) support in Dovecot.

					When this is enabled Sieve scripts may be uploaded by users
					using the ManageSieve protocol. In addition the
					`"modoboa-sievefilter"` extension can be enabeld to allow
					users to modify their Sieve filters using the web interface
					instead.
				'';

				options.enableQuota = mkDisableOption ''
					Whether to enable user storage quota support in Dovecot.

					This uses the Modoboa database for Quota tracking and is
					incompatible with enabling the
					{option}`services.dovecot2.enableQuota` setting.

					Without this the web interface always shows 0 storage use
					for all users and its quota settings (only available to
					admins) will not have any effect.
				'';
			};
			default = {};
		};
		
		postfix = lib.options.mkOption {
			type = lib.types.submodule {
				options.enable = mkDisableOption ''
					Whether to configure Postfix with Modoboa-related settings.

					Without this Modoboa will probably not be very useful.
				'';
			};
			default = {};
		};

		radicale = lib.options.mkOption {
			type = lib.types.submodule {
				options.enable = mkEnableOption ''
					Whether to configure Radicale with Modoboa-related settings.

					This enables the Radicale rights file integration that
					allows Modoboa to manage Radicale calendar groups as well
					as the Modoboa Radicale extension to display Radicale
					calendars and contacts. After enabling this flag the
					Radicale server still needs to be exposed using some
					web server (unless {option}`services.modoboa.caddy.exposeRadicale`
					is used) and its URL configured in the Modoboa settings
					(*Modoboa* > *Parameters* > *Radicale* > *Server URL*)
					in order for the integration to work.
				'';
			};
			default = {};
		};
		
		rspamd = lib.options.mkOption {
			type = lib.types.submodule {
				options.enable = mkDisableOption ''
					Whether to configure rspamd with Modoboa-related settings.

					This enables the DKIM signing integration that allows
					Modoboa to manage the DKIM signing keys for rspamd. Enabled
					by default since modern email with DKIM signing is not
					applicable for most setups.
				'';
			};
			default = {};
		};

		extensions = lib.options.mkOption {
			type = lib.types.listOf lib.types.str;
			default = [];
			description = ''
				Names of Python packages to add as Modoboa extensions.

				In accordance with the naming conventions used by Modoboa this
				adds a Python package with the name of the extension to the
				Modoboa Python environment and the name of the package with
				minuses (-) replaced by underscores (_) to the list of
				installed Modoboa apps. See the 
				{option}`services.modoboa.settings.EXTRA_MODOBOA_APPS` option
				for details what this means.

				Some known package names, such as “modoboa-rspamd”,
				may also cause related configuration changes to the Modoboa,
				Postfix or Dovecot configuration to be applied.
			'';
			example = [
				# Note: Try to list all supported extensions here to improve
				#       their visibility!
				"modoboa-contacts"
				"modoboa-radicale"
				"modoboa-rspamd"
				"modoboa-webmail"
			];
		};

		pythonPackage = lib.options.mkPackageOption pkgs "Python" {
			default = [ "python3" ];
			example = lib.options.literalExpression ''
				pkgs.python3.override {
					packageOverrides = final: prev: {
						modoboa = import ./modoboa-custom.nix;
					};
				};
			'';
		};

		extraPackages = lib.options.mkOption {
			type = lib.types.listOf lib.types.package;
			default = [];
			description = ''
				List of extra Nix system packages to add to the Modoboa runtime
				environment.
			'';
			example = lib.options.literalExpression "[ pkgs.bash ]";
		};

		extraPythonPackages = lib.options.mkOption {
			type = lib.types.mkOptionType {
				name = "Function that evaluates to a list of Python packages given a Python package set";
				check = value: lib.isFunction value;
				merge = loc: defs: (pp: lib.lists.foldl' (acc: def: acc ++ (def.value pp)) [] defs);
			};
			default = pp: [];
			description = ''
				List of extra Python packages to add to the Modoboa runtime
				environment, the package set of the used Python interpreter
				(ie `pythonPackage.pkgs`) will be passed as an argument
			'';
			example = lib.options.literalExpression ''
				pp: [
					pp.httpx
				]
			'';
		};


		user = lib.options.mkOption {
			type = lib.types.nonEmptyStr;
			default = "modoboa";
			description = ''
				The system user to use to run Modoboa services.
			'';
		};

		group = lib.options.mkOption {
			type = lib.types.nonEmptyStr;
			default = "modoboa";
			description = ''
				The system group to use to run Modoboa services.
			'';
		};


		mailUser = lib.options.mkOption {
			type = lib.types.nonEmptyStr;
			default = "mail";
			description = ''
				The system user to use for storing emails and associated data
				for Modoboa users.

				This also sets the Dovecot {option}`services.dovecot2.mailUser`
				setting.
			'';
		};
		
		mailGroup = lib.options.mkOption {
			type = lib.types.nonEmptyStr;
			default = "mail";
			description = ''
				The system group to use for storing emails and associated data
				for Modoboa users.

				This also sets the Dovecot {option}`services.dovecot2.mailGroup`
				setting.
			'';
		};
	};

	
	config = 
	let
		# Helper: Deduplicate list items
		dedup =
			list: lib.lists.foldl' (acc: item: acc ++ lib.lists.optional (!lib.lists.elem item acc) item) [] list;

		# Helper: Workaround to remove leading indentation from generated files
		#         independant of indentation type
		fixIdentWorkaround =
			string:
			let
				prefix = lib.lists.head (lib.strings.match "([[:space:]]+).*" string);
			in
				lib.strings.concatMapStringsSep "\n" (line: lib.strings.removePrefix prefix line) (lib.strings.splitString "\n" string);
		
		# Helper: Escape string for use inside a Python string literal
		escapePyString =
			string: "\"" + (lib.strings.escape ["\\" "\"" "'" "\n"] string) + "\"";

		# Create effective extension set based on specified extensions and
		# enabled options requiring integration extensions to work (rspamd)
		#
		# Duplicate items cause hard to understand errors during derivation
		# build and don’t really make sense, so we deduplicate the list before
		# use.
		extensions = dedup (cfg.extensions ++ (
			lib.optional cfg.rspamd.enable "modoboa-rspamd"
		) ++ (
			lib.optional cfg.radicale.enable "modoboa-radicale"
		));

		# Determine Dovecot/Postfix database driver name for Django database
		# engine name
		databaseDriver =
			if cfg.settings.DATABASES.default.ENGINE == "django.db.backends.mysql" then "mysql" else
			if cfg.settings.DATABASES.default.ENGINE == "django.db.backends.postgresql" then "pgsql" else
			if cfg.settings.DATABASES.default.ENGINE == "django.db.backends.sqlite3" then "sqlite" else
			throw "Unexpected default database engine value";

		# Apply default settings not managed directly in the settings key
		settings = {
			# Name of user used to store messages to run `doveadm` as
			#
			# If this does not match the user executing Modoboa it will attempt
			# to execute `doveadm` using `sudo` to this username. It’s pretty
			# fucked up, yes.
			DOVECOT_USER = cfg.user;
		} // cfg.settings // {
			# Extend `EXTRA_MODOBOA_APPS` to also include specified extensions
			EXTRA_MODOBOA_APPS = cfg.settings.EXTRA_MODOBOA_APPS ++ (
				lib.lists.map (name: lib.strings.replaceStrings ["-"] ["_"] name) extensions
			);
		};

		# Python virtual environment including all required Python packages
		pythonEnv = cfg.pythonPackage.withPackages(pp: [
			# Application server component
			pp.gunicorn
			
			# WSGI application with database dependencies
			pp.modoboa
		] ++ (
			# Add database dependencies for the selected DB driver
			lib.optionals (databaseDriver == "pgsql") pp.modoboa.optional-dependencies.postgresql ++
			lib.optionals (databaseDriver == "mysql") pp.modoboa.optional-dependencies.mysql
		) ++ (
			# Look up Python module with same name for listed extensions
			#
			# This skips over missing Python packages as those are reported
			# using a module-level assertion instead.
			lib.lists.foldl' (acc: name:
				acc ++ lib.lists.optional (lib.attrsets.hasAttr name pp) (lib.attrsets.getAttr name pp)
			) [] extensions
		) ++ (
			# Add user-selected extra modules
			cfg.extraPythonPackages pp
		));

		# List of packages to make available to all Modoboa services
		packages = with pkgs; [
			# Python virtual environment
			pythonEnv

			# Modoboa uses the `which` command for path look-ups of other binaries
			which

			# Statistics used for the mail statistics, both through Python
			# bindings and the standalone binary
			rrdtool

			# OpenSSL command used for DKIM key generation
			openssl
			
			# Access to Dovecot binary required to check its version
			dovecot
		] ++ cfg.extraPackages;
		
		siteSettings = pkgs.writeText "modoboa-site-settings.py" ((fixIdentWorkaround ''
			# Define special mode, induced by environment variable, that avoids
			# any external file access during the instance derivation build
			import os

			# Apply NixOS configuration settings
			#
			# Round-tripping it through JSON, since that takes care of all the
			# possible escaping issues while still preserving the structure.
			import json
			globals().update(json.loads(r"""${builtins.toJSON settings}"""))

			# Add all the apps from `EXTRA_MODOBOA_APPS` to the regular
			# apps lists so that we don’t need to duplicate those long lists
			# in the NixOS configuration
			#
			# The `MODOBOA_APPS` lists is used by Modoboa to automatically
			# apply URLconfs, while the `INSTALLED_APPS` list is used by the
			# Django framework.
			MODOBOA_APPS += tuple(EXTRA_MODOBOA_APPS)
			INSTALLED_APPS += tuple(EXTRA_MODOBOA_APPS) + tuple(EXTRA_INSTALLED_APPS)

			# Read password for each database from password file from disk
			#
			# This avoids copying the contents of the password file into
			# the Nix store where they would be world-readable.
			if not BUILDING_INSTANCE:
				for database in DATABASES.values():
					if (pw_filepath := database.get("PASSWORD_FILE")) is not None:
						with open(pw_filepath) as pw_file:
							database["PASSWORD"] = pw_file.read()

			# Automatically apply custom configuration hook from all apps
			# listed in `EXTRA_MODOBOA_APPS`
			import importlib
			for name in EXTRA_MODOBOA_APPS:
				try:
					importlib.import_module(f"{name}.settings").apply(globals())
				except (ModuleNotFoundError, AttributeError):
					pass  # Module did not have any settings hook

			# NixOS custom configuration
			''
		) + cfg.extraConfig);
		
		# Derive Modoboa instance configuration directory including collected
		# static files and the new frontend
		instanceDir = cfg.pythonPackage.pkgs.modoboa.mkInstance pythonEnv dataDir siteSettings;
	in lib.mkIf cfg.enable {
		assertions = [
			# Quota support conflicts
			{
				assertion = !cfg.dovecot.enableQuota || !config.services.dovecot2.enableQuota;
				message = "Modoboa Quota management (services.modoboa.dovecot.enableQuota) is "
				        + "incompatible with Dovecot Quota management (services.dovecot2.enableQuota)";
			}

			# Removed extensions
			{
				assertion = !lib.lists.elem "modoboa-postfix-autoreply" extensions;
				message = "The modoboa-postfix-autoreply extension has been merged into the "
				        + "Modoboa core package in Modoboa 2.3.0+, please remove its entry "
				        + "from `services.modoboa.extensions` to continue";
			}
			{
				assertion = !lib.lists.elem "modoboa-sievefilters" extensions;
				message = "The modoboa-sievefilters extension has been merged into the "
				        + "Modoboa core package in Modoboa 2.3.0+, please remove its entry "
				        + "from `services.modoboa.extensions` to continue";
			}

			# Missing extensions
			(let
				missingExtensions = lib.lists.filter (name:
					!lib.attrsets.hasAttr name cfg.pythonPackage.pkgs
					&& name != "modoboa-postfix-autoreply"  # already reported above
					&& name != "modoboa-sievefilters"       # already reported above
				) extensions;
			in {
				assertion = (missingExtensions == []);
				message = "The `services.modoboa.extensions` options contains extension "
				        + "names that do not correspond to any python package known for "
				        + "the Python version referenced by `services.modoboa.pythonPackage`: "
				        + (lib.strings.concatStringsSep " " missingExtensions);
			})
		];

		# Configure user account with preinstalled package environment
		users.users."${cfg.user}" = lib.mkIf (cfg.user == "modoboa") {
			isSystemUser = true;
			group = "${cfg.group}";
			home = dataDir;
			homeMode = "711"; # Allow limited access for service 
			createHome = true;
			
			packages = packages;
		};

		users.groups."${cfg.group}" = lib.mkIf (cfg.group == "modoboa") {};

		# Temporary directory for Modoboa-related sockets
		systemd.tmpfiles.settings."10-modoboa" = {
			"${runtimeDir}" = {
				d = {
					user  = cfg.user;
					group = cfg.group;
					mode  = "0755";
				};
			};
		};

		# Configure Dovecot instance
		#
		# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html
		services.dovecot2 = lib.mkIf cfg.dovecot.enable {
			enable = true;
			inherit (cfg) mailUser mailGroup;
			modules = lib.optional cfg.dovecot.enableSieve pkgs.dovecot_pigeonhole;

			# Define default mailboxes
			#
			# These settings are automatically applied to the “inbox” namespace.
			#
			# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#mailboxes
			mailboxes = {
				Drafts = {
					auto = "subscribe";
					specialUse = "Drafts";
				};
				Junk = {
					auto = "subscribe";
					specialUse = "Junk";
				};
				Sent = {
					auto = "subscribe";
					specialUse = "Sent";
				};
				Trash = {
					auto = "subscribe";
					specialUse = "Trash";
					autoexpunge = "30d";
				};
			};
			
			# Enable LMTP (required) as well as Sieve/ManageSieve and Quota
			# support if not disabled by user
			#
			# This does not use the existing NixOS “services.dovecot2.enableQuota”
			# setting as that presumes that the quota values are provided by the
			# “quota-status” service, while Modoboa instead manages it in the
			# database through the “dict” service.
			#
			# IMAP Sieve support (mail filtering during IMAP actions) is enabled
			# in addition to the usual Modoboa configuration since its absence
			# is probably an omission and in any case doesn’t break anything.
			#
			# Other Sieve and Quota configuration is done in “extraConfig” below.
			#
			# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#lmtp
			# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#managesieve-sieve
			# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#quota
			enableLmtp = true;
			enableQuota = lib.mkDefault false;

			globalSettings.auth_mechanisms = [
				(dovecotHelpers.mkStrSubst "auth_mechanisms")  # default value
				"oauthbearer"
				"xoauth2"
            ];

			globalSettings.protocols = (
				lib.optional cfg.dovecot.enableSieve "sieve"
			);

			mailPlugins.globally.enable = (
				lib.optional cfg.dovecot.enableQuota "quota"
			);

			mailPlugins.perProtocol.lmtp.enable = (
				lib.optional cfg.dovecot.enableSieve "sieve"
			) ++ (
				lib.optional cfg.dovecot.enableQuota "quota"
			);
			
			mailPlugins.perProtocol.imap.enable = [
			] ++ (
				lib.optional cfg.dovecot.enableSieve "imap_sieve"
			) ++ (
				lib.optional cfg.dovecot.enableQuota "imap_quota"
			);


			extraConfig = lib.concatStrings [
				# Enable user authentication for Modoboa users
				#
				# Uses the SQL configuration file generated above.
				#
				# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#authentication
				''
					passdb sql {
						driver = sql
						args = ${dovecotHelpers.formatValue "${runtimeDir}/dovecot-conf-exts/dovecot-sql.conf.ext"}
					}

					passdb oauth2 {
						driver = oauth2
						mechanisms = xoauth2 oauthbearer
						args = ${dovecotHelpers.formatValue "${runtimeDir}/dovecot-conf-exts/dovecot-oauth2.conf.ext"}
					}

					userdb prefetch {
						driver = prefetch
					}
					
					userdb sql {
						driver = sql
						args = ${dovecotHelpers.formatValue "${runtimeDir}/dovecot-conf-exts/dovecot-sql.conf.ext"}
					}
				''

				#XXX: Only use system passdb for local users
				#
				# Requires declaring everything in one block, so we’d need to
				# be able to merge these on the Nix level.
#				(lib.optionalString config.services.dovecot2.enablePAM ''
#					passdb system {
#						username_filter = "!*@*"
#					}
#				'')
				
				# Enable LMTP Unix Domain Socket listener for Postfix
				#
				# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#lmtp
				(lib.optionalString cfg.postfix.enable ''
					service lmtp {
						unix_listener /var/lib/postfix/queue/private/dovecot-lmtp {
							mode = 0600
							user = ${dovecotHelpers.formatValue config.services.postfix.user}
							group = ${dovecotHelpers.formatValue config.services.postfix.group}
						}
					}
				'')

				# Expose extra auth-client socket for Postfix to authenticate to
				(lib.optionalString cfg.postfix.enable ''
					service auth {
						unix_listener /var/lib/postfix/queue/private/auth-client {
							mode = 0600
							user = ${dovecotHelpers.formatValue config.services.postfix.user}
							group = ${dovecotHelpers.formatValue config.services.postfix.group}
						}
					}
				'')

				# Enable and configure the “dict” service to access the quota
				# from the database
				#
				# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#quota
				(lib.optionalString cfg.dovecot.enableQuota ''
					service dict {
						unix_listener dict {
							mode = 0600
							user = ${dovecotHelpers.formatValue cfg.mailUser}
							group = ${dovecotHelpers.formatValue cfg.mailGroup}
						}
					}
					
					dict {
						quota = ${dovecotHelpers.formatValue "${databaseDriver}:${runtimeDir}/dovecot-conf-exts/dovecot-dict-sql.conf.ext"}
					}

					plugin {
						quota = dict:User quota::proxy::quota
					}
				'')

				# Enable and configure the ManageSieve listener and Sieve protocol
				#
				# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#managesieve-sieve
				(lib.optionalString cfg.dovecot.enableSieve ''
					service managesieve-login {
						inet_listener sieve {
							port = 4190
						}
					}

					service managesieve {
						# nothing
					}

					plugin {
						sieve = ~/.dovecot.sieve
						sieve_dir = ~/sieve
						sieve_plugins = sieve_imapsieve
					}
				'')

				# Enable last-login tracking
				#
				# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/dovecot.html#last-login-tracking
				(lib.optionalString cfg.dovecot.enableLastLogin ''
					service imap {
						executable = imap postlogin
					}

					service pop3 {
						executable = pop3 postlogin
					}

					service postlogin {
						executable = ${dovecotHelpers.formatValue "script-login ${instanceDir}/postlogin.py"}
						user = ${dovecotHelpers.formatValue cfg.user}
						group = ${dovecotHelpers.formatValue cfg.group}
						unix_listener postlogin {
							# nothing
						}
					}
				'')

				# Expose extra auth-client socket for Radicale to authenticate to
				(lib.optionalString cfg.radicale.enable ''
					service auth {
						unix_listener radicale-auth-client {
							mode = 0600
							user = radicale
						}
					}
				'')
			];
		};

		# Configure Postfix instance
		services.postfix = lib.optionalAttrs cfg.postfix.enable {
			enable = true;

			# Add Modoboa configuration options
			#
			# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/postfix.html
			# Source: https://github.com/modoboa/modoboa-installer/blob/master/modoboa_installer/scripts/files/postfix/main.cf.tpl
			config = {
				unknown_local_recipient_reject_code = lib.mkDefault "550";
				unverified_recipient_reject_code = lib.mkDefault "550";
				
				# Appending .domain is the MUA's job.
				append_dot_mydomain = lib.mkDefault false;

				# Proxy maps
				proxy_read_maps = lib.mkBefore [
					"proxy:unix:passwd.byname"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-domains.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-domain-aliases.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-aliases.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-relaydomains.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-maintain.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-relay-recipient-verification.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-sender-login-map.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-spliteddomains-transport.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-transport.cf"
				];
			} // (lib.optionalAttrs cfg.dovecot.enable {
				# Virtual transport settings
				virtual_transport = lib.mkDefault "lmtp:unix:private/dovecot-lmtp";

				virtual_mailbox_domains = lib.mkBefore [
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-domains.cf"
				];
				virtual_alias_domains = lib.mkBefore [
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-domain-aliases.cf"
				];
				virtual_alias_maps = lib.mkBefore [
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-aliases.cf"
				];

				# SASL authentication through Dovecot
				smtpd_sasl_type = lib.mkDefault "dovecot";
				smtpd_sasl_path = lib.mkDefault "private/auth-client";
				smtpd_sasl_auth_enable = lib.mkDefault true;
				smtpd_sasl_security_options = lib.mkDefault ["noanonymous"];

				# Verify cache setup
				address_verify_map = lib.mkBefore [
					"proxy:btree:$data_directory/verify_cache"
				];
			}) // {

				# Relay domains
				relay_domains = lib.mkBefore [
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-relaydomains.cf"
				];
				transport_maps = lib.mkBefore ([
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-transport.cf"
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-spliteddomains-transport.cf"
				
				# Support for DMARC report parsing
				#
				# Source: https://modoboa.readthedocs.io/en/latest/configuration.html#dmarc-reports
				] ++ (lib.optional (cfg.dmarcAddress != null)
					"hash:/etc/postfix/dmarc_transport"
				));

				# Require HELO to use it in subsequent checks
				smtpd_helo_required = lib.mkDefault true;

				# Prevent trivial checking of whether an email address is valid
				disable_vrfy_command = lib.mkDefault true;

				# Require properly formatted message envelopes
				strict_rfc821_envelopes = lib.mkDefault true;

				# List of authorized senders
				smtpd_sender_login_maps = lib.mkBefore [
					"proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-sender-login-map.cf"
				];

				# Recipient restriction rules
				smtpd_recipient_restrictions = lib.mkAfter [
					"check_policy_service inet:localhost:9999"  #FIXME: This should use a Unix domain socket
					"permit_mynetworks"
					"permit_sasl_authenticated"
					"check_recipient_access proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-maintain.cf proxy:${databaseDriver}:${runtimeDir}/postfix-maps/sql-relay-recipient-verification.cf"
					"reject_unverified_recipient"
					"reject_unauth_destination"
					"reject_non_fqdn_sender"
					"reject_non_fqdn_recipient"
					"reject_non_fqdn_helo_hostname"
				];

				# See below …
				maillog_file = lib.mkDefault maillogFile;
				maillog_file_rotate_suffix = lib.mkDefault "bak";  # Always rotate file to /var/log/mail.log.bak
				maillog_file_compressor = lib.mkDefault "true";    # Disable compressing file
			};

			# Enable logging of Postfix postlogd to generate a mail.log to make
			# the Modoboa mail statistics panel work
			#
			# Basically we have Postfix write its logs to /var/log/mail.log, then
			# in the cron job have Modoboa’s log parser read those, then ask
			# Postfix to rotate them to /var/log/mail.log.bak without compression,
			# then delete the rotated file. This way it’s ensured that the mail
			# log files are only ever contain the unparsed mail logs not yet read
			# by Modoboa but nothing else.
			#
			# Reference: https://www.postfix.org/MAILLOG_README.html
			masterConfig = {
				postlog = {
					type = "unix-dgram";
					private = false;
					chroot = false;
					maxproc = 1;
					command = "postlogd";
				};

				autoreply = {
					type = "unix";
					privileged = true;
					chroot = false;
					command = "pipe";
					args = [
						"flags="
						"user=${cfg.mailUser}:${cfg.group}"
						"argv=env" "PATH=${lib.strings.makeBinPath packages}" "${instanceDir}/manage.py" "autoreply" "$sender" "$mailbox"
					];
				};

			# Support for DMARC report parsing
			#
			# Source: https://modoboa.readthedocs.io/en/latest/configuration.html#dmarc-reports
			} // (lib.optionalAttrs (cfg.dmarcAddress != null) {
				dmarc-rua-parser = {
					type = "unix";
					privileged = true;
					chroot = false;
					command = "pipe";
					args = [
						"flags="
						"user=${cfg.mailUser}:${cfg.group}"
						"argv=env" "PATH=${lib.strings.makeBinPath packages}" "${instanceDir}/manage.py" "import_aggregated_report" "--pipe"
					];
				};
			});

			aliasFiles = lib.optionalAttrs (cfg.dmarcAddress != null) {
				dmarc_transport = builtins.toFile "postfix-dmarc-transport" (fixIdentWorkaround ''
					${cfg.dmarcAddress}	dmarc-rua-parser:
				'');
			};
		};

		# Loose Modoboa rspamd integration
		#
		# rspamd is told where to expect the map files from Modoboa and its
		# Postfix integration is enabled if the Modoboa Postfix integration
		# was.
		#
		# In addition the Modoboa “modoboa-rspamd” extension is enabled and
		# configured to ensure that Modoboa will actually write the rspamd
		# map files (the extension does nothing else).
		services.rspamd = lib.optionalAttrs cfg.rspamd.enable {
			enable = lib.mkDefault true;

			postfix = lib.optionalAttrs cfg.postfix.enable {
				enable = lib.mkDefault true;
			};

			locals."dkim_signing.conf".text = lib.mkBefore ''
				selector_map = "${dataDir}/rspamd/dkim_selectors.map";
				path_map = "${dataDir}/rspamd/dkim_paths.map";

				# Header/Envelope/Username do not necessarily match when users
				# use aliases to send messages
				allow_hdrfrom_mismatch = true;
				allow_username_mismatch = true;
			'';
		};

		# Modoboa Radicale integration
		services.radicale = lib.optionalAttrs cfg.radicale.enable {
			enable = lib.mkDefault true;

			# Configure Radicale for Modoboa
			#
			# Source: https://github.com/modoboa/modoboa-installer/blob/master/modoboa_installer/scripts/files/radicale/config.tpl
			settings = {
				# Use Modoboa-generate rights file
				#
				# This does not use {option}`services.radicale.rightsFile`
				# since that option persumes the rights are provided as Nix
				# expression rather than a runtime file path.
				rights.type = lib.mkDefault "from_file";
				rights.file = lib.mkDefault "${dataDir}/radicale/rights.ini";

				# Authenticate using Dovecot auth RPC (plugin)
				auth.type = lib.mkDefault "radicale_dovecot_auth";
				auth.auth_socket = lib.mkDefault "/run/dovecot2/radicale-auth-client";

				# Enable optimized Radicale storage index (plugin)
				storage.type = lib.mkDefault "radicale_storage_by_index";
				storage.radicale_storage_by_index_fields = lib.mkDefault [ "dtstart" "dtend" "uid" "sum" ];
			};

			# Add Modoboa-specific plugins to Radicale
			#
			# Source: https://github.com/modoboa/modoboa-installer/blob/master/modoboa_installer/scripts/radicale.py#L32-L43
			package = pkgs.radicale.overrideDerivation(prev: {
				propagatedBuildInputs = prev.propagatedBuildInputs ++ [  #FIXME: Plugins should only be added to output
					pkgs.python3Packages.radicale-dovecot-auth
					pkgs.python3Packages.radicale-storage-by-index
				];
			});
		};

		systemd.services.radicale = {
			# Dovecot is needed for the exposed auth socket
			after = ["dovecot2.service"];
			requires = ["dovecot2.service"];

			# Make Dovecot auth socket and Modoboa rights file available to Radicale
			serviceConfig = {
				ReadOnlyPaths = lib.mkAfter ["${dataDir}/radicale" "/run/dovecot2"];
				RestrictAddressFamilies = lib.mkAfter [ "AF_UNIX" ];
			};
		};

		# Configure Redis instance for Modoboa
		services.redis.servers.modoboa = {
			enable = true;
			user = "${cfg.user}";
			unixSocket = "/run/redis-modoboa/redis.sock";
			unixSocketPerm = 660;
		};

		# Configure Caddy as reverse proxy
		services.caddy.virtualHosts = lib.optionalAttrs cfg.caddy.enable {
			modoboa = 
			let
				hostNames =
					if cfg.caddy.hostNames != null
					then
						cfg.caddy.hostNames
					else
						cfg.settings.ALLOWED_HOSTS;
			in {
				hostName = lib.mkDefault (lib.lists.head hostNames);
				serverAliases = lib.mkBefore (lib.lists.tail hostNames);

				extraConfig = lib.mkAfter ((lib.optionalString (cfg.radicale.enable && cfg.caddy.exposeRadicale) ''
					handle_path /radicale/* {  #*/
						reverse_proxy * http://localhost:5232 {
							header_up X-Script-Name /radicale
						}
					}

					# Well-known redirects for automatic service discovery
					# in DAV clients
					redir /.well-known/caldav  /radicale/ permanent
					redir /.well-known/carddav /radicale/ permanent
				'') + ''
					# Serve static files
					@staticpath path /sitestatic/* /media/*  #*/
					handle @staticpath {
						file_server {
							root ${instanceDir}
							browse
						}
					}
					
					handle_path /new-admin/* {  #*/
						root * ${dataDir}/frontend
						
						header Pragma        "no-cache"
						header Cache-Control "no-store, no-cache, must-revalidate, post-check=0, pre-check=0"

						try_files {path} {path}/ /index.html =404
						file_server
					}

					# Add trailing slash to new admin interface URL
					redir /new-admin /new-admin/

					# Forward all other requests to the Modoboa gunicorn service
					reverse_proxy * unix/${runtimeDir}/gunicorn.sock
				'');
			};
		};

		# Configure cron jobs
		#
		# Source: https://modoboa.readthedocs.io/en/latest/manual_installation/modoboa.html#cron-jobs
		services.cron = {
			enable = lib.mkDefault true;
			systemCronJobs = 
			let
				#NB: The emitted FutureWarnings are rather unimportant but they
				#    cause a warning email to be dispatched every time the job
				#    is executed nonetheless
				manageCmd  = lib.strings.escapeShellArgs ["env" "PATH=${lib.strings.makeBinPath packages}" "PYTHONWARNINGS=ignore::FutureWarning" "${instanceDir}/manage.py"];
				postfixCmd = lib.strings.escapeShellArgs ["${pkgs.postfix}/bin/postfix"];
			in lib.mkBefore [
				#NB: The handle_mailbox_operations cronjob is implemented as
				#    systemd timer+service instead
				
				# Generate DKIM keys (they will belong to the user running this job)
				#
				# Unlike upstream this does not set a umask (making the
				# generated world-readable), since we restrict the containing
				# directory to only be accessible by rspamd instead.
				"*     *  *  *  *  ${cfg.user}  ${manageCmd} modo manage_dkim_keys"
				
				# Sessions table cleanup (XXX: actually root)
				"0     0  *  *  *  ${cfg.user}  ${manageCmd} clearsessions"
				# Logs table cleanup (XXX: actually root)
				"0     0  *  *  *  ${cfg.user}  ${manageCmd} cleanlogs"
				# Logs parsing
				#
				# Since we only created the log file to allow Modoboa to parse it,
				# we ask Postfix to rotate the logfile immediately after Modoboa
				# has finished doing that then remove the rotated file. This should
				# minimize the disk usage file the mail.log file to what is
				# minimally needed to make Modoboa’s log parser work.
				"*/15  *  *  *  *  root         ${manageCmd} logparser && ${postfixCmd} logrotate && rm ${maillogFile}.bak"
				"0     *  *  *  *  ${cfg.user}  ${manageCmd} update_statistics"
				# DNSBL checks
				"*/30  *  *  *  *  ${cfg.user}  ${manageCmd} modo check_mx"
				# Public API communication
				"0     *  *  *  *  ${cfg.user}  ${manageCmd} communicate_with_public_api"

				# Radicale extension: Generate radicale rights file
				#
				# Source: https://modoboa-radicale.readthedocs.io/en/latest/setup.html
				(lib.optionalAttrs (lib.lists.elem "modoboa-radicale" extensions)
					"*/2   *  *  *  *  ${cfg.user}  ${manageCmd} generate_rights"
				)
			];
		};

		# Make `modoboa-manage` command available globally
		environment.systemPackages = [
			instanceDir
		];

		# Configure services
		systemd.sockets.modoboa = {
			wantedBy = [ "multi-user.target" ];

			# Require Modoboa initialization steps before creating the socket
			requires = [ "modoboa-init.service" ];
			after = [ "modoboa-init.service" ];
			
			listenStreams = [ "${runtimeDir}/gunicorn.sock" ];

			socketConfig = {
				SocketUser = cfg.user;
				SocketGroup = cfg.group;
				SocketMode = "0666";
			};
		};

		# The handle_mailbox_operations cron is implemented as systemd timer
		# allow the task to be executed with supplementary groups set
		systemd.timers.modoboa-handle-mailbox-operations = {
			description = "Modoboa – Handle filesystem changes to mailboxes";
			
			# Start and stop together with Modoboa socket
			requiredBy = [ "modoboa.socket" ];
			partOf = [ "modoboa.socket" ];
			
			# Require Modoboa initialization steps before creating the timer
			requires = [ "modoboa-init.service" ];
			after = [ "modoboa-init.service" ];

			timerConfig = {
				OnCalendar = "minutely";
			};
		};
		
		systemd.services = {
			# Modoboa Web frontend
			#
			# Source 1: https://docs.gunicorn.org/en/latest/deploy.html#systemd
			# Source 2: https://modoboa.readthedocs.io/en/latest/manual_installation/webserver.html#id4
			modoboa = {
				description = "Modoboa – Mail hosting made simple (Web frontend)";

				# Start other service unit along with this one
				requires = [ "modoboa.socket" "modoboa-policyd.service" "modoboa-rq.service" ];
				after = [ "modoboa.socket" ];
				
				path = packages;

				serviceConfig = {
					Type = "notify";
					ExecStart = utils.escapeSystemdExecArgs [
						"${pythonEnv}/bin/python" "-m" "gunicorn" "instance.wsgi:application" "--capture-output" "--log-syslog" "--log-level" "debug"
					];
					ExecReload = "${utils.escapeSystemdExecArg "${pkgs.procps}/bin/kill"} -sHUP $MAINPID";
					WorkingDirectory = instanceDir;
					KillMode = "mixed";
						
					User = "${cfg.user}";
					Group = "${cfg.group}";
					RuntimeDirectory = "modoboa";
					RuntimeDirectoryPreserve = true;
					RuntimeDirectoryMode = "0755";
					TimeoutStopSec = 5;
					PrivateTmp = true;
				};
			};

			modoboa-init = {
				description = "Modoboa – Mail hosting made simple (Pre-start initialization)";
				
				# Wait for the database server to start up if it is installed,
				# but don’t fail if it isn’t
				requires = [ "redis-modoboa.service" ];
				after = [ "redis-modoboa.service" "mysql.service" "mariadb.service" "postgresql.service" ];

				# Make Dovecot and Postfix wait on this unit since their config
				# references values generated by it
				before = [ "postfix.service" "dovecot.service" ];
				requiredBy = [ "postfix.service" "dovecot.service" ];

				# Remove the Modoboa server socket when stopping this unit
				partOf = [ "modoboa.socket" ];
				
				path = packages;

				serviceConfig = {
					Type = "oneshot";
					ExecStart = lib.lists.map utils.escapeSystemdExecArgs ([
						# Ensure used directories exist since Modoboa tends
						# to crash if they don’t
						(["${pkgs.coreutils}/bin/mkdir" "-p" "${dataDir}/dkim-keys" "${dataDir}/media" "${dataDir}/stats"] ++ (
							lib.optional (lib.lists.elem "modoboa-rspamd" extensions) "${dataDir}/rspamd"
						) ++ (
							lib.optional (lib.lists.elem "modoboa-radicale" extensions) "${dataDir}/radicale"
						))

						# Set owning group of directories containing
						# configuration for other services so that the service
						# user of that service is able read them
						#
						# This works in conjunction with the set UMask to
						# ensure that the effective access permissions of the
						# directory will end being:
						#   * User: modoboa
						#   * Group: <other-service-group>
						#   * Access: u=rwx,g=rx,o= (0750)
						# (Writable by Modoboa, readable by other service.)
					] ++ (lib.optional (lib.lists.elem "modoboa-rspamd" extensions)
						["${pkgs.coreutils}/bin/chgrp" "${config.services.rspamd.group}" "${dataDir}/dkim-keys" "${dataDir}/rspamd"]
					) ++ (lib.optional (lib.lists.elem "modoboa-radicale" extensions)
						["${pkgs.coreutils}/bin/chgrp" "radicale" "${dataDir}/radicale"]
					) ++ [

						# Ensure proper system-specific secret and OIDC keys
						# are generated
						["${instanceDir}/extras/init-data-directory.py" "${dataDir}"]

						# Apply pre-migration database upgrades where needed
						#
						# This primarily just runs `./manage.py migrate`,
						# however it takes care to also perform advanced
						# migration techniques if required for weird upgrade
						# cases that require special case (“stop-releases”).
						["${instanceDir}/extras/migrate.py"]
						
						# Synchronize system paths with Modoboa database
						(
							["${instanceDir}/extras/apply-system-configuration.py"
								"--dkim-keys-path" "${dataDir}/dkim-keys"
								"--maillog-paths" maillogFile "${dataDir}/stats"
								"--pdf-credentials-path" "${dataDir}/pdf_credentials"
							]  ++ (lib.optionals (lib.lists.elem "modoboa-rspamd" extensions) [
								"--rspamd-map-paths" "${dataDir}/rspamd/dkim_paths.map" "${dataDir}/rspamd/dkim_selectors.map"
							]) ++ (lib.optionals (lib.lists.elem "modoboa-radicale" extensions) [
								"--radicale-rights-path" "${dataDir}/radicale/rights.ini"
							])
						)

						# Generate Postfix map files containing database access
						# information
						#
						# Changes the permissions as mentioned above to make
						# the generated files writable by Modoboa and readable
						# by Postfix.
						["${instanceDir}/manage.py" "generate_postfix_maps" "--destdir" "${runtimeDir}/postfix-maps"]
						["${pkgs.coreutils}/bin/chgrp" "-R" "${config.services.postfix.group}" "${runtimeDir}/postfix-maps"]

						# Generate Dovecot config extension files containing
						# database access information
						#
						# Changes the permissions as mentioned above to make
						# the generated files writable by Modoboa and readable
						# by Dovecot.
						["${instanceDir}/extras/generate-dovecot-conf-exts.py" "${runtimeDir}/dovecot-conf-exts" "${dataDir}" "${lib.strings.replaceStrings ["%"] ["%%"] mailDir}/%u/state"]
						["${pkgs.coreutils}/bin/chgrp" "-R" "${config.services.dovecot2.group}" "${runtimeDir}/dovecot-conf-exts"]

						# Run deployment checks (just prints some info)
						["${instanceDir}/manage.py" "check" "--deploy"]
					]);
					RemainAfterExit = true;

					# Run as Modoboa user
					User = "${cfg.user}";
					Group = "${cfg.group}";
					
					# Allow generated database service files to be assigned to
					# their respective service users
					AmbientCapabilities = ["CAP_CHOWN"];

					# Prevent unnecessary file access bits in generated files
					# but preserve them across restarts
					UMask = "0027";  # = ~0750
					RuntimeDirectory = "modoboa";
					#RuntimeDirectoryPreserve = true;
					RuntimeDirectoryMode = "0755";
					PrivateTmp = true;
				};
			};
			
			# Postfix Policy daemon
			modoboa-policyd = {
				description = "Modoboa Postfix Policy daemon";
				wantedBy = [ "multi-user.target" ];
				requires = [ "redis-modoboa.service" "modoboa-init.service" ];
				after = [ "redis-modoboa.service" "modoboa-init.service" ];
				path = packages;

				serviceConfig = {
					Type = "exec";
					ExecStart = utils.escapeSystemdExecArgs ["${instanceDir}/manage.py" "policy_daemon"];
					WorkingDirectory = instanceDir;
		
					User = "${cfg.user}";
					Group = "${cfg.group}";
				};
			};

			# RQ job handler daemon
			modoboa-rq = {
				description = "Modoboa RQ job handler daemon";
				wantedBy = [ "multi-user.target" ];
				requires = [ "redis-modoboa.service" "modoboa-init.service" ];
				after = [ "redis-modoboa.service" "modoboa-init.service" ];
				path = packages;

				serviceConfig = {
					Type = "exec";
					ExecStart = utils.escapeSystemdExecArgs ["${instanceDir}/manage.py" "rqworker" "modoboa" "dkim"];
					WorkingDirectory = instanceDir;
		
					User = "${cfg.user}";
					Group = "${cfg.group}";
				};
			};

			# The handle_mailbox_operations cronjob (see timer description)
			modoboa-handle-mailbox-operations = {
				description = "Modoboa – Handle filesystem changes to mailboxes";

				requires = [ "redis-modoboa.service" "modoboa-init.service" ];
				after = [ "redis-modoboa.service" "modoboa-init.service" ];

				path = packages;

				serviceConfig = {
					ExecStart = utils.escapeSystemdExecArgs ["${instanceDir}/manage.py" "handle_mailbox_operations"];

					# Needs to run as mailbox user, but still needs access to
					# Modoboa configuration
					User = cfg.mailUser;
					SupplementaryGroups = [ cfg.group ];
				};
			};
		};
	};

	meta = {
		maintainers = with lib.maintainers; [ ];
		#doc = ./modoboa.md;
	};
}